const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const SECRET_STRING = process.env.SECRET_STRING;
const express = require('express');
const videoModule = require('../modules/video');
const articleModule = require('../modules/article');
const commentModule = require('../modules/comment');
const userModule = require('../modules/user');
const authModule = require('../modules/authentication');
const translationModule = require('../modules/translation');
const translationExportModule = require('../modules/translationExport');
const organizationModule = require('../modules/organization');
const invitationsModule = require('../modules/invitationsResponse');
const notificationModule = require('../modules/notification');
const subtitlesModule = require('../modules/subtitles');
const noiseCancellationVideoModule = require('../modules/noiseCancellationVideo');
const apiKeyModule = require('../modules/apiKey');
const videoTutorialContributionModule = require('../modules/videoTutorialContribution');

const userService = require('../modules/shared/services/user');
const apiKeyService = require('../modules/shared/services/apiKey');
const emailService = require('../modules/shared/services/email');

const PUBLIC_ROUTES = [
  /^\/api\/organization\/(.)*\/invitations\/respond$/,
  /^\/api\/user\/resetPassword$/,
  /^\/api\/user\/subscribe_api_docs$/,
]

module.exports = (app) => {
  // Decode uri component for all params in GET requests
  app.get('*', (req, res, next) => {
    if (req.query) {
      Object.keys(req.query).forEach((key) => {
        req.query[key] = decodeURIComponent(req.query[key]);
      })
    }

    return next();
  });

  app.use('/api/auth', authModule.routes.mount(createRouter()));
  app.use('/api/invitations', invitationsModule.routes.mount(createRouter()));
  // Upload contribute video
  app.use('/api/videoTutorialContribution', videoTutorialContributionModule.routes.mount(createRouter()));

  app.use(async (req, res, next) => {
    let token = req.header('x-access-token');
    // Skip public routes regexs
    if (PUBLIC_ROUTES.some(s => req.path.match(s))) return next();

    if (token) {
      jwt.verify(token, SECRET_STRING, (err, decoded) => {

        if (err) {
          return res.json({
            success: false,
            message: 'Token is not valid'
          });
        } else {
          req.decoded = decoded;
          let userData;
          userService.getUserByEmail(decoded.email)
            .then((user) => {
              req.user = user;
              userData = user;
              next();
              return Promise.resolve();
            })
            .then(() => {
              // BACKWARD COMPATABILITY
              // if the user doesn't have an associated api key, create one for him
              if (!userData.apiKey) {
                userData.organizationRoles.forEach((role) => {
                  apiKeyService.findOne({ user: req.user._id, organization: role.organization._id })
                    .then((apiKey) => {
                      if (!apiKey) {
                        apiKeyService.create({
                          organization: role.organization._id,
                          user: userData._id,
                          key: apiKeyService.generateApiKey(),
                          origins: [role.organization.name.replace(/\s/g, '-').toLowerCase() + '.' + process.env.FRONTEND_HOST_NAME],
                          active: true,
                          userKey: true,
                        })
                          .then((apiKey) => {
                            console.log('created api key', apiKey)
                          })
                          .catch(err => {
                            console.log('error creating api key', err)
                          })
                      }
                    })
                    .catch(err => {
                      console.log('error api key', err)
                    })
                })
                // organization: { type: Schema.Types.ObjectId, ref: SchemaNames.organization },
                // user: { type: Schema.Types.ObjectId, ref: SchemaNames.user },

                // key: String,
                // origins: [String],
                // active: { type: Boolean, default: true },

                // created_at: { type: Number, default: Date.now },
                // console.log(process.env)
              }
            })
            .catch((err) => {
              console.log(err);
              return res.json({ success: false, message: 'Something went wrong' })
            })
        }
      });
    } else {
      const apiKeyVal = req.header('vw-x-user-api-key');
      if (apiKeyVal) {
        apiKeyService.findOne({ key: apiKeyVal })
          .populate('user')
          .then((apiKey) => {
            if (!apiKey) return res.status(400).send('Invalid api key');
            userService.getUserByEmail(apiKey.user.email)
              .then(user => {
                req.user = user;
                return next();
              })
              .catch(err => {
                console.log(err)
                return next();
              })
          })
      } else {
        next();
      }
    }
  });

  /* Server Routes */
  app.use('/api/user', userModule.routes.mount(createRouter()));

  app.use('/api/video', videoModule.routes.mount(createRouter()));
  app.use('/api/article', articleModule.routes.mount(createRouter()));
  app.use('/api/comment', commentModule.routes.mount(createRouter()));
  app.use('/api/translate', translationModule.routes.mount(createRouter()));
  app.use('/api/translationExport', translationExportModule.routes.mount(createRouter()));
  app.use('/api/organization', organizationModule.routes.mount(createRouter()))
  app.use('/api/notification', notificationModule.routes.mount(createRouter()))
  app.use('/api/subtitles', subtitlesModule.routes.mount(createRouter()))
  app.use('/api/noiseCancellationVideo', noiseCancellationVideoModule.routes.mount(createRouter()));
  app.use('/api/apikey', apiKeyModule.routes.mount(createRouter()));

  app.get('/*', (req, res) => {
    res.status(404).send('Not found');
  });

}

function createRouter() {
  return express.Router()
}

setTimeout(() => {
  console.log(process.env.GOOGLE_APPLICATION_CREDENTIALS)
}, 5000);