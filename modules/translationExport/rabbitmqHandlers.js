
const rabbitmqService = require('../shared/vendors/rabbitmq');
const queues = require('../shared/vendors/rabbitmq/queues');
const videoService = require('../shared/services/video');
const articleService = require('../shared/services/article');
const subtitlesService = require('../shared/services/subtitles');
const socketConnectionService = require('../shared/services/socketConnection');
const translationExportService = require('../shared/services/translationExport');
const websockets = require('../shared/vendors/websockets');
const websocketsRooms = require('../shared/vendors/websockets/rooms');
const websocketsEvents = require('../shared/vendors/websockets/events');

const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
let rabbitmqChannel;

function init() {
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        rabbitmqChannel.prefetch(1);
        rabbitmqChannel.consume(queues.ARCHIVE_ARTICLE_TRANSLATION_AUDIOS_FINISH, onArchiveAudiosFinish, { noAck: false });
        rabbitmqChannel.consume(queues.GENERATE_ARTICLE_TRANSLATION_VIDEO_SUBTITLE_FINISH, onGenerateSubtitleFinish, { noAck: false });
        rabbitmqChannel.consume(queues.BURN_ARTICLE_TRANSLATION_VIDEO_SUBTITLE_FINISH, onBurnSubtitlesFinish, { noAck: false });
        rabbitmqChannel.consume(queues.EXPORT_ARTICLE_TRANSLATION_FINISH, onExportTranslationFinish, { noAck: false });
            
    });
}

function onExportTranslationFinish(msg) {
    const { translationExportId } = JSON.parse(msg.content.toString());
    console.log('onExportTranslationFinish', translationExportId)
    rabbitmqChannel.ack(msg);
   
    let translationExport;
    let article;
    translationExportService.findById(translationExportId)
    .populate('article')
    .then((translationExportDoc) => {
        if (!translationExportDoc) throw new Error('Invalid translation export id' + translationExportId);
        translationExport = translationExportDoc.toObject();
        if (translationExport.status === 'failed') throw new Error('Failed translation export')
        article = translationExport.article;
        return subtitlesService.find({ article: translationExport.article._id });
    })
    .then((subtitles) => {
        // if there's no subtitles generated after the first export, generate one
        if (!subtitles || subtitles.length === 0) {
            const subtitles = subtitlesService.generateSubtitlesFromSlides(article.slides)
            const newSubtitles = {
                article: article._id,
                organization: article.organization,
                video: article.video,
                subtitles,
            }
            return subtitlesService.create(newSubtitles)
        }
    })
    .then(() => {
        console.log('onExportTranslationFinish done');
    })
    .catch((err) => {
        console.log('error onExportTranlationFinish', err);
    })
}

function onArchiveAudiosFinish(msg) {
    const { translationExportId } = JSON.parse(msg.content.toString());
    let translationExport;
    rabbitmqChannel.ack(msg);

    translationExportService.findById(translationExportId)
    .populate('audioArchiveBy')
    .then((translationExportDoc) => {
        if (!translationExportDoc) throw new Error('Invalid translation export id');
        translationExport = translationExportDoc.toObject();
        if (!translationExport.audioArchiveBy) throw new Error('No one requested to download that');

        return socketConnectionService.find({ userEmail: translationExport.audioArchiveBy.email })
    })
    .then((socketConnections) => {
        if (!socketConnections || socketConnections.length === 0) {
            return console.log('User is offline');
        }

        const socketConnection = socketConnections[0].toObject()
        console.log('emitting download file event', socketConnection, translationExport)
        websockets.ioEmitter.to(socketConnection.socketId).emit(websocketsEvents.DOWNLOAD_FILE, { url: translationExport.audiosArchiveUrl })
    })
    .catch((err) => {
        console.log(err);
    })
}

function onGenerateSubtitleFinish(msg) {
    const { translationExportId } = JSON.parse(msg.content.toString());
    let translationExport;
    rabbitmqChannel.ack(msg);

    translationExportService.findById(translationExportId)
    .populate('subtitleBy')
    .then((translationExportDoc) => {
        if (!translationExportDoc) throw new Error('Invalid translation export id');
        translationExport = translationExportDoc.toObject();
        if (!translationExport.subtitleBy) throw new Error('No one requested to download that');

        return socketConnectionService.find({ userEmail: translationExport.subtitleBy.email })
    })
    .then((socketConnections) => {
        if (!socketConnections || socketConnections.length === 0) {
            return console.log('User is offline');
        }

        const socketConnection = socketConnections[0].toObject()
        console.log('emitting download file event', socketConnection, translationExport)
        websockets.ioEmitter.to(socketConnection.socketId).emit(websocketsEvents.DOWNLOAD_FILE, { url: translationExport.subtitleUrl })
    })
    .catch((err) => {
        console.log(err);
    }) 
}


function onBurnSubtitlesFinish(msg) {
    const { translationExportId } = JSON.parse(msg.content.toString());
    let translationExport;
    rabbitmqChannel.ack(msg);
    console.log('downloading burn video')
    translationExportService.findById(translationExportId)
    .populate('subtitledVideoBy')
    .then((translationExportDoc) => {
        if (!translationExportDoc) throw new Error('Invalid translation export id');
        translationExport = translationExportDoc.toObject();
        if (!translationExport.subtitledVideoBy) throw new Error('No one requested to download that');

        return socketConnectionService.find({ userEmail: translationExport.subtitledVideoBy.email })
    })
    .then((socketConnections) => {
        if (!socketConnections || socketConnections.length === 0) {
            return console.log('User is offline');
        }

        const socketConnection = socketConnections[0].toObject()
        console.log('emitting download file event', socketConnection, translationExport)
        websockets.ioEmitter.to(socketConnection.socketId).emit(websocketsEvents.DOWNLOAD_FILE, { url: translationExport.subtitledVideoUrl })
    })
    .catch((err) => {
        console.log(err);
    }) 
}


module.exports = {
    init,
}