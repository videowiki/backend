const routes = require('./routes');
const rabbitmqHandlers = require('./rabbitmqHandlers');
rabbitmqHandlers.init()

module.exports = {
    routes,
}