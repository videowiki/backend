const async = require('async');
const uuid = require('uuid').v4;

const articleService = require('../shared/services/article');
const translationExportService = require('../shared/services/translationExport');
const notificationService = require('../shared/services/notification');
const translationService = require('../shared/services/translation');
const userService = require('../shared/services/user');


const audioProcessor = require('../shared/workers/audio_processor');
const exporterWorker = require('../shared/workers/exporter');

const emailService = require('../shared/services/email');


const controller = {
  getByArticleId: function (req, res) {
    const { articleId } = req.params;
    let { page } = req.query;
    console.log('page is ', page)
    const perPage = 5;
    if (page) {
      page = parseInt(page);
    } else {
      page = 1;
    }
    const skip = page === 1 ? 0 : (page * perPage - perPage);

    let translationExports;
    translationExportService.find({ article: articleId })
      .sort({ created_at: -1 })
      .skip(skip)
      .limit(perPage)
      .populate('exportRequestBy', 'firstname lastname email')
      .populate('translationBy', 'firstname lastname email')
      .populate('approvedBy', 'firstname lastname email')
      .populate('declinedBy', 'firstname lastname email')
      .then((te) => {
        translationExports = te;
        return translationExportService.count({ article: articleId });
      })
      .then((count) => {
        return res.json({ translationExports, pagesCount: Math.ceil(count / perPage) });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send(err.message);
      })
  },

  exportTranslationRequest: function (req, res) {
    const { articleId } = req.body;
    let article;
    let newTranslationExport;
    const user = req.user;
    articleService.findById(articleId)
      .then((a) => {
        if (!a) throw new Error('Invalid article id');
        article = a;

        const { valid, message } = translationService.validateArticleExport(article);
        if (!valid) {
          throw new Error(message)
        }

        return translationExportService.find({ article: article._id, exportRequestStatus: 'pending' })
      })
      .then((pendingRequests) => {
        if (pendingRequests && pendingRequests.length > 0) throw new Error('An export request is already pending approval');

        const subslides = articleService.cleanArticleSilentAndBackgroundMusicSlides(article).slides.reduce((acc, s) => acc.concat(s.content), []);
        const translationByIds = [];
        subslides.forEach((subslide) => {
          if (subslide.audioUser && translationByIds.indexOf(subslide.audioUser.toString()) === -1) {
            translationByIds.push(subslide.audioUser.toString());
          }
        })
        const translationExportItem = {
          organization: article.organization,
          video: article.video,
          article: article._id,
          exportRequestStatus: 'pending',
          exportRequestBy: user._id,
          translationBy: translationByIds,
        };

        return translationExportService.create(translationExportItem)
      })
      .then((translationExport) => {
        return new Promise((resolve) => {
          res.json({ translationExport });
          newTranslationExport = translationExport;
          if (!article.translators || article.translators.length === 0) return resolve();
          // TODO: notify admins and verifiers with export request
          let articleAdminsIds = article.translators.map((t) => t.invitedBy);
          if (article.verifiers && article.verifiers.length > 0) {
            articleAdminsIds = articleAdminsIds.concat(article.verifiers);
          }
          // Filter duplicates
          articleAdminsIds = articleAdminsIds.filter((v,i) => articleAdminsIds.indexOf(v) === i);
          if (articleAdminsIds.length === 0) return resolve();
          articleAdminsIds
          .map((adminId) => adminId.toString())
            .filter((adminId) => adminId !== user._id.toString())
            .forEach((adminId) => {
            const notificationData = {
              owner: adminId,
              from: user._id,
              organization: article.organization,
              type: 'translation_export_request',
              content: `${user.email} has requested an export review on the video translation "${article.title}" (${article.langCode || article.langName})`,
              resource: article._id,
              resourceType: 'article',
            };
            notificationService.notifyUser({ _id: adminId, organization: article.organization.toString() }, notificationData)
              .then((data) => {
                console.log('notified admin', data)
                return userService.findById(adminId)
              })
              .then((userData) => {
                emailService.inviteUserToVerifyTranslation({
                  from: req.user,
                  to: userData,
                  articleId,
                  
                })
              })
              .catch((err) => {
                console.log('error notifying admin', err);
              })
          })
          resolve();
        })
      })
      // Set the version number
      .then(() => {
        return new Promise((resolve) => {
          // Skip 1 to skip the newely created export
          console.log('translatin export versioning')
          translationExportService.find({ article: article._id }).sort({ created_at: -1 }).skip(1).limit(1)
            .then((latestExports) => {
              let latestExport;
              if (latestExports.length > 0) {
                 latestExport = latestExports[0];
              }
              let version = 1;
              let subVersion = 0;
              if (latestExport) {
                // If it was exported, then it's a subversion update
                // Else it's a version update
                if (article.exported) {
                  version = latestExport.version || 1;
                  subVersion = (latestExport.subVersion || 0) + 1;
                } else {
                  version = (latestExport.version || 0) + 1;
                  subVersion = 0;
                }
              }
              console.log('versions', version, subVersion)
              return translationExportService.update({ _id: newTranslationExport._id }, { version, subVersion });
            })
            .then(() => articleService.update({ _id: article._id }, { exported: true }))
            .then(() => {
              resolve();
            })
            .catch(err => {
              resolve();
              console.log('error setting version', err);
            })
        })
      })
      .catch(err => {
        console.log(err);
        return res.status(400).send(err.message);
      })
  },

  approveTranslationExport: function (req, res) {
    const { translationExportId } = req.params;
    let translationExport;
    let article;
    translationExportService.findById(translationExportId)
      .then((te) => {
        if (!te) throw new Error('Invalid translation export id');
        translationExport = te;
        return articleService.findById(translationExport.article)
          .populate('video');
      })
      .then((articleDoc) => {
        if (!articleDoc) throw new Error('Invalid article id');
        article = articleDoc.toObject();
        const { valid, message } = translationService.validateArticleExport(article);
        if (!valid) {
          return res.status(400).send(message)
        }
        return translationExportService.find({ article: article._id, status: { $in: ['queued', 'processing'] } });
      })
      .then((progressingTranslationExports) => {
        return new Promise((resolve, reject) => {
          if (progressingTranslationExports && progressingTranslationExports.length === 1 && progressingTranslationExports[0]._id.toString() === translationExportId) {
            return resolve();
          }

          if (progressingTranslationExports && progressingTranslationExports.length > 0) {
            return reject(new Error('This video is already being exported'));
          }
          return resolve();
        })
      })
      .then(() => {
        const subslides = articleService.cleanArticleSilentAndBackgroundMusicSlides(article).slides.reduce((acc, s) => acc.concat(s.content), []);
        const translationByIds = [];
        subslides.forEach((subslide) => {
          if (subslide.audioUser && translationByIds.indexOf(subslide.audioUser.toString()) === -1) {
            translationByIds.push(subslide.audioUser.toString());
          }
        })
        const translationExportUpdate = {
          status: 'queued',
          exportRequestStatus: 'approved',
          approvedBy: req.user._id,
          translationBy: translationByIds,
          dir: uuid(),
          hasBackgroundMusic: Boolean(article.video.backgroundMusicUrl),
          backgroundMusicTransposed: article.video.backgroundMusicTransposed,
        }

        return translationExportService.update({ _id: translationExport._id }, translationExportUpdate)
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send(err.message);
      })
      .then(() => {
        res.json({ success: true, message: 'The video has been queued to be exported' });
        // Process audios
        return new Promise((resolve, reject) => {
          const processAudioFuncArray = [];
          const cleanedArticle = articleService.cleanArticleSilentAndBackgroundMusicSlides(article)
          cleanedArticle.slides.forEach(slide => {
            slide.content.forEach((subslide) => {
              if (!subslide.silent && subslide.speakerProfile && subslide.speakerProfile.speakerNumber !== -1 && !subslide.audioProcessed && subslide.audio) {
                processAudioFuncArray.push((cb) => {
                  audioProcessor.processRecordedAudioViaApi(article._id, slide.position, subslide.position)
                    .then((response) => {
                      console.log('processed audio', response);
                      cb(null, response)
                    })
                    .catch(err => cb(err));
                })
              }
            })
          });
          async.parallelLimit(processAudioFuncArray, 2, (err, result) => {
            console.log(err, result);
            if (err) return reject(err);
            exporterWorker.exportArticleTranslation(translationExport._id);
            resolve()
          })
        })
      })
      .catch(err => {
        console.log(err);
        translationExportService.update({ _id: translationExport._id }, { status: 'failed' })
          .then(() => { })
          .catch((err) => console.log(err));
      })
  },

  declineTranslationExport: function (req, res) {
    const { translationExportId } = req.params;
    translationExportService.update({ _id: translationExportId }, { exportRequestStatus: 'declined', declinedBy: req.user._id })
      .then(() => {
        return res.json({ success: true });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send(err.message);
      })
  },

  archiveAudios: function(req, res) {
    const { translationExportId } = req.params;
    translationExportService.update({ _id: translationExportId }, { audiosArchiveProgress: 10, audioArchiveBy: req.user._id })
    .then(() => {
      exporterWorker.archiveArticleTranslationAudios(translationExportId);
      return translationExportService.findById(translationExportId)
            .populate('exportRequestBy', 'email')
            .populate('translationBy', 'email')
            .populate('approvedBy', 'email')
            .populate('declinedBy', 'email')
    })
    .then((translationExport) => {
      return res.json({ translationExport, queued: true });
    })
    .catch((err) => {
      console.log(err);
      return res.status(400).send('Something went wrong');
    })
  },
  
  generateVideoSubtitle: function(req, res) {
    
    const { translationExportId } = req.params;
    translationExportService.update({ _id: translationExportId }, { subtitleProgress: 30, subtitleBy: req.user._id })
      .then(() => {
        exporterWorker.generateTranslatedArticleSubtitles(translationExportId);
        return translationExportService.findById(translationExportId)
              .populate('exportRequestBy', 'email')
              .populate('translationBy', 'email')
              .populate('approvedBy', 'email')
              .populate('declinedBy', 'email')
      })
      .then((translationExport) => {
        return res.json({ queued: true, translationExport });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send('Something went wrong');
      })
  },

  burnVideoSubtitle: function(req, res) {
    
    const { translationExportId } = req.params;
    translationExportService.update({ _id: translationExportId }, { subtitledVideoProgress: 30, subtitledVideoBy: req.user._id })
      .then(() => {
        exporterWorker.burnTranslatedArticleVideoSubtitle(translationExportId);
        return translationExportService.findById(translationExportId)
              .populate('exportRequestBy', 'email')
              .populate('translationBy', 'email')
              .populate('approvedBy', 'email')
              .populate('declinedBy', 'email')
      })
      .then((translationExport) => {
        return res.json({ queued: true, translationExport });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send('Something went wrong');
      })
  },

  burnVideoSubtitleAndSignlanguage: function(req, res) {
    const { translationExportId } = req.params;
    const { articleId } = req.body;
    translationExportService.update({ _id: translationExportId }, { subtitledSignlanguageVideoProgress: 10, signLanguageArticle: articleId })
      .then(() => {
        exporterWorker.burnTranslatedArticleVideoSubtitleAndSignlanguage(translationExportId);
        return translationExportService.findById(translationExportId)
              .populate('exportRequestBy', 'email')
              .populate('translationBy', 'email')
              .populate('approvedBy', 'email')
              .populate('declinedBy', 'email')
      })
      .then((translationExport) => {
        return res.json({ queued: true, translationExport });
      })
      .catch((err) => {
        console.log(err);
        return res.status(400).send('Something went wrong');
      })
  },


  updateVoiceVolume: function(req, res) {
      const { translationExportId } = req.params;
      const { voiceVolume } = req.body;
      translationExportService.update({ _id: translationExportId }, { voiceVolume })
      .then(() => {
          return res.json({ voiceVolume });
      })
      .catch((err) => {
          console.log(err);
          return res.status(400).send(err.message);
      })
  },


  updateAudioSettings: function(req, res) {
      const { translationExportId } = req.params;
      const { voiceVolume, backgroundMusicVolume, normalizeAudio } = req.body;
      const changes = {};
      if (typeof voiceVolume !== 'undefined') {
        changes.voiceVolume = parseFloat(voiceVolume).toFixed(2)
      }
      if (typeof backgroundMusicVolume !== 'undefined') {
        changes.backgroundMusicVolume = parseFloat(backgroundMusicVolume).toFixed(2)
      }
      if (typeof normalizeAudio !== 'undefined') {
        changes.normalizeAudio = normalizeAudio
      }
      translationExportService.update({ _id: translationExportId }, changes)
      .then(() => {
          return res.json(changes);
      })
      .catch((err) => {
          console.log(err);
          return res.status(400).send(err.message);
      })
  },


}

module.exports = controller;
