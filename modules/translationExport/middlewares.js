const translationExportService = require('../shared/services/translationExport');
const articleService = require('../shared/services/article');
// const organizationService = require

const middlewares = {
    authorizeApproveAndDecline: (req, res, next) => {
        const { translationExportId } = req.params;
        translationExportService.findById(translationExportId)
        .then((translationExport) => {
            if (!translationExport) throw new Error('invalid id');
            const organizationRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === translationExport.organization.toString());
            console.log('organization rol', organizationRole)
            if (!organizationRole) return res.status(401).send('Unauthorized');
            if (organizationRole.permissions.indexOf('admin') !== -1 || organizationRole.organizationOwner)
                return next();
                
            articleService.findById(translationExport.article)
            .then((articleDoc) => {
                const article = articleDoc.toObject();
                const verifiers = article.verifiers && article.verifiers.length > 0 ? article.verifiers.map(v => v.toString()) : [];
                if (verifiers.length === 0 || verifiers.indexOf(req.user._id.toString()) === -1) return res.status(401).send('Unauthorized'); 
                return next();
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },
    authorizeRequestExport: (req, res, next) => {
        const { articleId } = req.body;
        articleService.findById(articleId)
        .then((article) => {
            if (!article) throw new Error('invalid id');
            const organizationRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === article.organization.toString());
            if (!organizationRole || (organizationRole.permissions.indexOf('admin') === -1 && organizationRole.permissions.indexOf('translate') === -1 && !organizationRole.organizationOwner)) {
                return res.status(401).send('Unauthorized');
            }
            return next();
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })   
    },
    validateArchiveAudios: (req, res, next) => {
        const { translationExportId } = req.params;
        translationExportService.findById(translationExportId)
        .then((translationExport) => {
            if (!translationExport) throw new Error('Invalid id');
            if (translationExport.audiosArchiveUrl) throw new Error('An archive has already been generated for this translation');
            if (translationExport.audiosArchiveProgress) throw new Error('Archiving is already in progress');
            return next();
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    validateGenerateSubtitles: (req, res, next) => {
        const { translationExportId } = req.params;
        translationExportService.findById(translationExportId)
        .then((translationExport) => {
            if (!translationExport) throw new Error('Invalid id');
            if (translationExport.subtitleUrl) throw new Error('A subtitled video has already been generated for this translation');
            if (translationExport.subtitleProgress) throw new Error('Burning subtitles is already in progress');
            return next();
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    validateBurnSubtitles: (req, res, next) => {
        const { translationExportId } = req.params;
        translationExportService.findById(translationExportId)
        .then((translationExport) => {
            if (!translationExport) throw new Error('Invalid id');
            if (translationExport.subtitledVideoUrl) throw new Error('A subtitled video has already been generated for this translation');
            if (translationExport.subtitledVideoProgress) throw new Error('Burning subtitles is already in progress');
            return next();
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    }
}
module.exports = middlewares;