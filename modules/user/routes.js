const controller = require('./controller');

const mount = function (router) {
    // router.post("/remove", controller.removeUser);
    // router.post('/editPermissions', controller.editPermissions);

    router.get("/getOrgUsers", controller.getOrgUsers);
    router.get("/getUserDetails", controller.getUserDetails);
    router.get("/isValidToken", controller.isValidToken);

    router.patch('/showUserGuiding', controller.updateShowUserGuiding)
    
    router.patch('/:userId/password', controller.updatePassword);
    router.post('/resetPassword', controller.resetPassword);

    router.post('/subscribe_api_docs', controller.subscriber_api_docs)
    return router;
};

module.exports = {
    mount,
};
