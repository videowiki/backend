const _ = require('lodash');
const uuid = require('uuid').v4;
const sha256 = require('sha256');

const userService = require('../shared/services/user');
const apiDocsSubscriberService = require('../shared/services/apiDocsSubscriber');
const authService = require('../shared/services/auth');
const emailService = require('../shared/services/email');
const organizationService = require('../shared/services/organization');
const { handlePromiseReject } = require('../shared/utils/helpers');

const controller = {
    updatePassword: function(req, res) {
        const { userId } = req.params;
        const { oldPassword, password, passwordConfirm } = req.body;

        if (!password || password !== passwordConfirm) return res.status(400).send('Passwords doesnt match');
        if (password.length < 8) return res.status(400).send('Password must be at least 8 characters');
        console.log(password, password.length)
        userService.findById(userId)
        .select('+password')
        .then((user) => {
            if (!user) throw new Error('Invalid user id ');
            if (user.password !== sha256(oldPassword)) throw new Error('Invalid old password');

            return userService.update({ _id: user._id }, { password: sha256(password), passwordSet: true });
        })
        .then(() => {
            return res.json({ success: true });
        })
        .catch(err => {
          console.log(err);
          return res.status(400).send(err.message);  
        })
    },

    resetPassword: function(req, res) {
        const { resetCode, email, password, passwordConfirm } = req.body;
        if (!password || password !== passwordConfirm) return res.status(400).send('Passwords doesnt match');
        if (password.length < 8) return res.status(400).send('Password must be at least 8 characters');
        
        let token;
        let user;
        let newPassword;
        userService.findOne({ email })
        .then((userDoc) => {
            if (!userDoc) throw new Error('Invalid user email');
            user = userDoc.toObject();
            console.log('user is', user);
            if (user.resetCode !== resetCode) throw new Error('Invalid reset code');

            newPassword = sha256(password);
            return userService.update({ _id: user._id }, { password: newPassword, passwordSet: true });
        })
        .then(() => {
            return authService.loginUser(email, password);
        })
        .then((t) => {
            token = t;
            return userService.getUserByEmail(email);
        })
        .then((userData) => {
            return res.json({ success: true, token, user: userData.toObject() });
        })
        .catch(err => {
          console.log(err);
          return res.status(400).send(err.message);  
        })
    },
    
    getOrgUsers: async function (req, res) {
        // let currentUserName = req.user.email;
        const { organization }  = req.query;
        // console.log(req.user, 'user is')
        // let loggedUser = await userService.getUserByEmail(currentUserName);
        // let organizationId = await userService.getOrgId(loggedUser);
        let userList = await userService.getOrganizationUsers(organization);

        res.status(200).send(userList);
    },

    getUserDetails: async function (req, res) {
        let currentUserName = req.user.email;
        let loggedUser = await userService.getUserByEmail(currentUserName);

        res.status(200).send({
            ...loggedUser.toObject(),
            firstname: loggedUser.firstname,
            lastname: loggedUser.lastname,
            languages: loggedUser.languages,
            email: loggedUser.email,
            emailVerified: loggedUser.emailVerified,
            registerMethod: loggedUser.registerMethod,
            inviteStatus: loggedUser.inviteStatus,
            organizationRoles: loggedUser.organizationRoles,
        });
    },

    isValidToken: async function (req, res) {
        let currentUserName = req.user.email;
        res.status(200).send({ isValid: !!currentUserName, user: req.user });
    },

    subscriber_api_docs: function (req, res) {
        const { email } = req.body;
        apiDocsSubscriberService.findOne({ email: email })
        .then((sub) => {
            if (sub) return res.json({ success: true });
            apiDocsSubscriberService.create({ email })
            .then(() => {
                return res.json({ success: true });
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
        })
        .catch(err => {
            console.log(err)
            return res.status(400).send(err.message);
        })
    },

    updateShowUserGuiding: function(req, res) {
        const user = req.user;
        const { showUserGuiding } = req.body;

        userService.update({ _id: user._id }, { showUserGuiding })
        .then(() => {
            return res.json({ showUserGuiding });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    }

};

module.exports = controller;
