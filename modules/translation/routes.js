const controller = require('./controller');
const middlewares = require('./middlewares');

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.mimetype.split('/').pop())
    }
})
var upload = multer({ storage: storage })

// external modules should call the mount function and pass it an instance 
// of the router to add the module's routes to it
const mount = function (router) {
    // Define module routes here
    router.post('/:articleId/text', middlewares.authorizeTranslationUpdate, controller.addTranslatedText);
    router.post('/:articleId/text/replace', middlewares.authorizeTranslationUpdate, controller.replaceTranslatedText);
    
    router.post('/:articleId/audio', upload.single('file'), middlewares.authorizeTranslationUpdate, controller.addRecordedAudio);
    router.post('/:articleId/picInPic', upload.single('file'), middlewares.authorizeTranslationUpdate, controller.addPictureInPicture);
    router.patch('/:articleId/picInPic/position', middlewares.authorizeTranslationUpdate, controller.updatePictureInPicturePosition);
    
    router.post('/:articleId/audio/tts', middlewares.authorizeTranslationUpdate, controller.generateTTSAudio);
    router.post('/:articleId/audio/original', middlewares.authorizeTranslationUpdate, controller.updateAudioFromOriginal);
    router.delete('/:articleId/audio', middlewares.authorizeTranslationUpdate, controller.deleteRecordedAudio);
    
    router.post('/:articleId/videoSpeed', controller.updateVideoSpeed);
    router.post('/:articleId', controller.generateTranslatableArticle);

    router.get('/:articleId/languages', controller.getTranslatableArticleLangs);
    // TODO: DOC THIS
    router.get('/:articleId', controller.getTranslatableArticle);
    
    return router;
}

module.exports = {
    mount,
}
