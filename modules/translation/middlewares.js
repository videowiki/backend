const articleService = require('../shared/services/article');
const { handlePromiseReject  } = require('../shared/utils/helpers');

const middlewares = {
    authorizeTranslationUpdate: async function (req, res, next) {
        const { articleId } = req.params;
        const { slidePosition, subslidePosition } = req.body;
        const { err, data: article } = await handlePromiseReject(articleService.findById(articleId));
        if (err) {
            return res.status(400).send(err.message)
        }
        if (!article) return res.staus(400).send('Invalid article');

        const userRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === article.organization.toString());
            console.log(userRole)
        if (!userRole) return res.stats(401).send('Unauthorized');
        // Admins can do whatever they want. duh!
        if (userRole.organizationOwner || userRole.permissions.indexOf('admin') !== -1) return next();

        if (!userRole.organizationOwner && userRole.permissions.indexOf('admin') === -1 && userRole.permissions.indexOf('translate') === -1) {
            return res.status(401).send('Unauthorized');
        }

        const slide = article.slides.find((s) => parseInt(slidePosition) === parseInt(s.position));
        if (!slide) return res.status(400).send('Invalid slide position');

        const subslide = slide.content.find((s) => parseInt(subslidePosition) === parseInt(s.position));
        if (!subslide) return res.status(400).send('Invalid subslide position');

        const { translators } = article;
        const subslideTranslationRole = translators.find((t) => t.speakerNumber === subslide.speakerProfile.speakerNumber)
        // If no-one is assigned to the translation, allow any translators to edit
        if (!subslideTranslationRole && userRole.permissions.indexOf('translate') !== -1) {
            return next();
        }
        if (!subslideTranslationRole) return res.status(400).send(`No users are assigned to this speaker ( Speaker ${subslide.speakerProfile.speakerNumber} )`);
        if (subslideTranslationRole.user.toString() !== req.user._id.toString()) {
            return res.status(400).send(`You're not assigned to translate for Speaker ${subslideTranslationRole.speakerNumber}`)
        }

        return next();
    }
}

module.exports = middlewares;