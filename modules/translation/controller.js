const uuid = require('uuid').v4;
const fs = require('fs');
const storageService = require('../shared/vendors/storage');
const articleService = require('../shared/services/article');
const userService = require('../shared/services/user');
const translationService = require('../shared/services/translation');
const translatorWorker = require('../shared/workers/translator');
const exporterWorker = require('../shared/workers/exporter');

const websocketsService = require('../shared/vendors/websockets');
const websocketsRooms = require('../shared/vendors/websockets/rooms');
const events = require('../shared/vendors/websockets/events');

const fileUtils = require('../shared/utils/fileUtils');
const { TRANSLATION_AUDIO_DIRECTORY, PICTURE_IN_PICTURE_DIRECTORY } = require('../shared/constants');

const controller = {

    generateTranslatableArticle: function(req, res) {
        const { articleId } = req.params;
        const { lang, tts, signLang } = req.body;
        let { langName } = req.body;
        console.log('body is', req.body)
        let originalArticle;
        let clonedArticle;

        if (!langName) {
            langName = '';
        }
       
        articleService.findById(articleId)
        .then((originalArticleDoc) => {
            if (!originalArticleDoc) throw new Error('Invalid article id');
            originalArticle = originalArticleDoc.toObject();
           
            const query = {
                originalArticle: originalArticle._id,
                archived: false,
            }
            if (signLang) {
                query.signLangCode = lang;
                query.signLang = true;
            } else {
                query.langCode = lang;
            }
            if (langName) {
                query.langName = langName;
            }
            if (tts) {
                query.tts = true;
            } else {
                query.tts = false;
            }
            return articleService.find(query)
        })
        .then((articleDoc) => {
            if (articleDoc && articleDoc.length > 0) {
                return res.json({ article: articleDoc[0].toObject() });
            }

            articleService.cloneArticle(articleId)
                .then((clonedArticleDoc) => {
                    clonedArticle = clonedArticleDoc;
                    if (clonedArticle.toObject) {
                        clonedArticle = clonedArticle.toObject();
                    }
                    clonedArticle.slides.forEach(slide => {
                        slide.content.forEach((subslide) => {
                            if (subslide.speakerProfile && subslide.speakerProfile.speakerNumber === -1) {
                            } else {
                                subslide.audio = '';
                            }
                        })
                    });
                    const newArticleUpdate = { articleType: 'translation', langName, slides: clonedArticle.slides, archived: false };
                    if (signLang) {
                        newArticleUpdate.signLang = true;
                        newArticleUpdate.langName = ''
                    }
                    newArticleUpdate.langCode = lang;
                    if (tts) {
                        newArticleUpdate.tts = true;
                    }
                    return articleService.update({ _id: clonedArticle._id }, newArticleUpdate);
                })
                .then(() => articleService.findById(clonedArticle._id))
                .then((clonedArticle) => {
                    return new Promise((resolve, reject) => {
                        // if ()
                        clonedArticle = clonedArticle.toObject();
                        if (!clonedArticle.signLang &&
                                clonedArticle.langCode !== originalArticle.langCode &&
                                originalArticle.langCode.indexOf(clonedArticle.langCode) !== 0
                            ) {
                            translatorWorker.translateArticleText({ articleId: clonedArticle._id, lang });
                            return resolve(clonedArticle);
                        } else {
                            articleService.update({ _id: clonedArticle._id }, { translationProgress: 100 })
                            .then(() => {
                                clonedArticle.translationProgress = 100;
                                return resolve(clonedArticle);
                            })
                            .catch(reject)                              
                        }
                    })
                })
                .then((article) => {
                    console.log('Created Article');
                    return res.json({ article: articleService.cleanArticleSilentAndBackgroundMusicSlides(article), originalArticle });
                })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getTranslatableArticleLangs: function(req, res) {
        const { articleId } = req.params;
        let originalLanguage;
        const languages = []
        articleService.findById(articleId)
        .populate('originalArticle')
        .then((article) => {
            if (!article) throw new Error('Oops, the video might have been deleted by the admin');
            if (article.articleType !== 'translation') return res.status(400).send('Article id should be of a translation article')
            originalLanguage = articleService.getArticleLanguage(article.originalArticle);

            return articleService.find({ originalArticle: article.originalArticle._id });
        })
        .then((articleTranslations) => {
            articleTranslations.filter((t) => t._id.toString() !== articleId).forEach((a) => {
                // Check if the article is already completed
                if (articleService.isArticleCompleted(a)) {
                    languages.push(articleService.getArticleLanguage(a));
                }
            })
            return res.json({ languages, originalLanguage })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })      
    },

    getTranslatableArticle: function (req, res) {
        const { articleId } = req.params;
        const { langCode, langName, tts } = req.query;
        let originalArticle;
        articleService
            .findById(articleId)
            .populate('originalArticle')
            .then((article) => {
                if (!article) return res.status(400).send('Oops, the video may have been deleted by the admin');
                article = article.toObject();
                if (article.articleType !== 'translation') return res.status(400).send('Article id should be of a translation article');

                // If neither the lang code or lang name is set
                // or the lang code/name is the same of the original article
                // give back the original article
                if ((!langCode && !langName) || (langCode === article.originalArticle.langCode && Boolean(tts) === Boolean(article.originalArticle.tts))) {
                    originalArticle = articleService.cleanArticleSilentAndBackgroundMusicSlides(article.originalArticle);
                    article.originalArticle = article.originalArticle._id;
                    return res.json({ article: articleService.cleanArticleSilentAndBackgroundMusicSlides(article), originalArticle });
                }
                // Get custom base language article
                const baseArticleQuery = {
                    originalArticle: article.originalArticle._id,
                    langCode,
                }
                if (langName) {
                    baseArticleQuery.langName = langName;
                }
                if (Boolean(tts)) {
                    baseArticleQuery.tts = true;
                } else {
                    // baseArticleQuery.tts = false;
                    baseArticleQuery['$or'] = [
                        { tts: false },
                        { tts: { $exists: false }}
                    ];
                }

                articleService.find(baseArticleQuery)
                .then((articles) => {
                    if (!articles || articles.length === 0) throw new Error(`No article with ${langCode}|${langName} as base language`);

                    originalArticle = articleService.cleanArticleSilentAndBackgroundMusicSlides(articles[0].toObject());
                    article.originalArticle = article.originalArticle._id
                    return res.json({ article: articleService.cleanArticleSilentAndBackgroundMusicSlides(article), originalArticle });
                })
                .catch(err => {
                    console.log(err);
                    return res.status(400).send(err.message);
                })
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    },

    addTranslatedText: function (req, res) {
        const { articleId } = req.params;
        const { slidePosition, subslidePosition, text } = req.body;
        articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, { text, audioSynced: false })
            .then((article) => {
                websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { text, audioSynced: false } });
                return res.json({ text, slidePosition, subslidePosition, audioSynced: false });
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
            .then(() => {
                articleService.update({ _id: articleId }, { exported: false })
                .then(() => {

                })
                .catch(err => {
                    console.log('error updating article exported', err);
                })
            })
    },

    replaceTranslatedText: function(req, res) {
        const { articleId } = req.params;
        const { find, replace } = req.body;
        console.log(req.body)
        let article;
        articleService.findById(articleId)
            .then((articleDoc) => {
                article = articleDoc.toObject();

                return articleService.replaceArticleSlidesText(articleId, { find, replace })
            })
            .then((changedSlides) => {
                changedSlides.forEach(({ slidePosition, subslidePosition, text }) => {
                    websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { text, audioSynced: false } });
                })
                return articleService.findById(articleId);
            })
            .then((article) => {
                return res.json({ article: articleService.cleanArticleSilentAndBackgroundMusicSlides(article.toObject())})
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
            .then(() => {
                articleService.update({ _id: articleId }, { exported: false })
                .then(() => {

                })
                .catch(err => {
                    console.log('error updating article exported', err);
                })
            })
    },

    updateAudioFromOriginal: function(req, res) {
        const { articleId } = req.params;
        let { slidePosition, subslidePosition } = req.body;
        slidePosition = parseInt(slidePosition);
        subslidePosition = parseInt(subslidePosition);
        let article;

        let audioPath;
        let audioUrl;
        let newAudioUrl;
        let fileName;
        articleService.findById(articleId)
        .populate('originalArticle')
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article id');
            article = articleDoc.toObject();
            if (article.articleType !== 'translation') throw new Error('This is only available for translation articles');
            const slide = article.originalArticle.slides.find((s) => s.position === slidePosition);
            if (!slide) throw new Error('Invalid slidePosition')
            const subslide = slide.content.find(s => s.position === subslidePosition);
            if (!subslide) throw new Error('Invalid subslide position');
            // Create a new clone for the audio file and upload it
            audioUrl = subslide.audio;

            return fileUtils.downloadFile(audioUrl)
        })
        .then((filePath) => {
            audioPath = filePath;
            fileName = `cloned_audio_${uuid()}.${audioUrl.split('.').pop()}`;
            return storageService.saveFile(TRANSLATION_AUDIO_DIRECTORY, fileName, fs.createReadStream(audioPath));
        })
        .then((uploadRes) => {
            const { data, url } = uploadRes;
            newAudioUrl = url;
            fs.unlink(audioPath, () => {
                
            })
            return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, {
                audio: url,
                audioKey: data.Key,
                audioFileName: fileName,
                audioUser: req.user._id,
                audioProcessed: false,
                audioSynced: true,
                audioSource: 'original',
            })
        })
        .then(() => {
            return res.json({ slidePosition, subslidePosition, audio: newAudioUrl, audioSynced: true, audioProcessed: false });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    addPictureInPicture: function (req, res) {
        const { file } = req;
        const { articleId } = req.params;
        let { slidePosition, subslidePosition } = req.body;
        slidePosition = parseInt(slidePosition);
        subslidePosition = parseInt(subslidePosition);
        if (!file) return res.status(400).send('Invalid file field');
        let uploadedVideoUrl;
        let article;

        const filePath = file.path;
        const fileExtension = file.mimetype.split('/').pop();
        const fileName = `video-${uuid()}.${fileExtension}`;
        let user;
        userService.getUserByEmail(req.user.email)
            .then((u) => {
                user = u;
                return articleService.findById(articleId)
            })
            .then((articleDoc) => {
                if (!articleDoc) throw new Error('Invalid article id');
                article = articleDoc;
                console.log('getting duration');
                return fileUtils.getFileDuration(filePath);

            })
            .then((duration) => {
                const slide = article.slides.find((s) => s.position === slidePosition);
                const subslide = slide.content.find((s) => s.position === subslidePosition);
                console.log('starting upload');
                console.log('duration is', duration, subslide.media[0].duration)
                if (subslide.media[0].duration < duration) {
                    throw new Error('Video duration should be less than or equal the video slide duration');
                }
                return storageService.saveFile(PICTURE_IN_PICTURE_DIRECTORY, fileName, fs.createReadStream(filePath))
            })
            .then((uploadRes) => {
                console.log('uploaded pic in pic', uploadRes);
                uploadedVideoUrl = uploadRes.url;
                // Delete previous picInPic if exists
                const slide = article.slides.find((s) => s.position === slidePosition);
                const subslide = slide.content.find((s) => s.position === subslidePosition);
                if (subslide.picInPicVideoUrl && subslide.picInPicFileName) {
                    console.log('supposed to be deleting', PICTURE_IN_PICTURE_DIRECTORY, subslide.picInPicFileName)
                    // storageService.deleteFile(PICTURE_IN_PICTURE_DIRECTORY, subslide.picInPicFileName)
                    //     .then(() => {
                    //         console.log('deleted file');
                    //     })
                    //     .catch((err) => {
                    //         console.log('error deleting file', err);
                    //     });
                }

                const articleUpdate = { 
                    picInPicVideoUrl: uploadRes.url,
                    picInPicKey: uploadRes.data.Key,
                    picInPicFileName: fileName,
                    picInPicUser: user._id,
                };
                return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, articleUpdate)
            })
            .then((doc) => {
                // audioProcessor.processRecordedAudio({ articleId, slidePosition, subslidePosition });

                // websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { picInPicVideoUrl: uploadedVideoUrl } });
                return res.json({ picInPicVideoUrl: uploadedVideoUrl, slidePosition, subslidePosition, audioSynced: true })
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message || 'Something went wrong');
            })
    },

    updatePictureInPicturePosition: function(req, res) {
        const { articleId } = req.params;
        let { slidePosition, subslidePosition, picInPicPosition } = req.body;
        slidePosition = parseInt(slidePosition);
        subslidePosition = parseInt(subslidePosition);
        articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, { picInPicPosition })
        .then(() => {
            return res.json({ slidePosition, subslidePosition, picInPicPosition });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message || 'Something went wrong');
        })
    },
    
    addRecordedAudio: function (req, res) {
        const { file } = req;
        const { articleId } = req.params;
        let { slidePosition, subslidePosition } = req.body;
        slidePosition = parseInt(slidePosition);
        subslidePosition = parseInt(subslidePosition);
        if (!file) return res.status(400).send('Invalid file field');
        let uploadedAudioUrl;
        let article;

        const filePath = file.path;
        const fileExtension = file.mimetype.split('/').pop();
        const fileName = `audio-${uuid()}.${fileExtension}`;
        let user;
        userService.getUserByEmail(req.user.email)
            .then((u) => {
                user = u;
                return articleService.findById(articleId)
            })
            .then((articleDoc) => {
                if (!articleDoc) throw new Error('Invalid article id');
                article = articleDoc;
                return fileUtils.getAudioDuration(filePath);
            })
            .then((duration) => {
                const slide = article.slides.find((s) => s.position === slidePosition);
                const subslide = slide.content.find((s) => s.position === subslidePosition);
                console.log('starting upload');
                console.log('duration is', duration)
                if (subslide.media[0].duration * 1000 < duration) {
                    throw new Error('Audio duration should be less than or equal the video slide duration');
                }
                return storageService.saveFile(TRANSLATION_AUDIO_DIRECTORY, fileName, fs.createReadStream(filePath))
            })
            .then((uploadRes) => {
                console.log('uploaded', uploadRes);
                uploadedAudioUrl = uploadRes.url;
                // Delete previous audio if exists
                const slide = article.slides.find((s) => s.position === slidePosition);
                const subslide = slide.content.find((s) => s.position === subslidePosition);
                if (subslide.audio && subslide.audioFileName) {
                    storageService.deleteFile(TRANSLATION_AUDIO_DIRECTORY, subslide.audioFileName)
                        .then(() => {
                            console.log('deleted file');
                        })
                        .catch((err) => {
                            console.log('error deleting file', err);
                        });
                }

                const articleUpdate = { 
                    audio: uploadRes.url,
                    audioKey: uploadRes.data.Key,
                    audioFileName: fileName,
                    audioUser: user._id,
                    audioProcessed: false,
                    audioSynced: true,
                    audioSource: 'user',
                };
                return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, articleUpdate)
            })
            .then((doc) => {
                // audioProcessor.processRecordedAudio({ articleId, slidePosition, subslidePosition });

                websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { audio: uploadedAudioUrl } });
                return res.json({ audio: uploadedAudioUrl, slidePosition, subslidePosition, audioSynced: true })
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message || 'Something went wrong');
            })
            .then(() => {
                articleService.update({ _id: articleId }, { exported: false })
                .then(() => {

                })
                .catch(err => {
                    console.log('error updating article exported', err);
                })
            })
    },

    deleteRecordedAudio: function (req, res) {
        const { articleId } = req.params;
        const { slidePosition, subslidePosition } = req.body;
        let article;
        articleService.findById(articleId)
            .then((a) => {
                if (!a) throw new Error('Invalid article id');
                article = a.toObject();
                return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, { audio: '', audioKey: '', audioFileName: '', audioSynced: false, audioSource: '' })
                
            })
            .catch((err) => {
                console.log(err);
                return res.status(400).send(err.message || 'Something went wrong');
            })
            .then((doc) => {
                res.json({ audio: '', slidePosition, subslidePosition, audioSynced: false })
                websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { audio: '' } });

                const slide = article.slides.find((s) => s.position === slidePosition);
                const subslide = slide.content.find((s) => s.position === subslidePosition);
                const { audioFileName } = subslide;
                return storageService.deleteFile(TRANSLATION_AUDIO_DIRECTORY, audioFileName)
            })
            
            .then((deleteRes) => {
                console.log('deleted', deleteRes);
            })
            .catch((err) => {
                console.log(err);
            })
    },

    generateTTSAudio: function(req, res) {
        const { articleId } = req.params;
        const { slidePosition, subslidePosition } = req.body;
        console.log('body is', req.body)
        let audioPath;
        let article;
        let uploadedAudioUrl;
        articleService.findById(articleId)
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article id');
            article = articleDoc.toObject();
            if (!article.tts) throw new Error('This feature is available only to tts articles');
            const { valid, message } = articleService.validateSlideAndSubslidePosition(article, slidePosition, subslidePosition);
            if (!valid) throw new Error(message);
            return translationService.generateSlideTextToSpeech(article, slidePosition, subslidePosition)
        })
        .then((generateAudioPath) => {
            audioPath = generateAudioPath;
            return storageService.saveFile(TRANSLATION_AUDIO_DIRECTORY, audioPath.split('/').pop(), fs.createReadStream(audioPath));
        })
        .then((uploadRes) => {
                uploadedAudioUrl = uploadRes.url;
                const articleUpdate = { 
                    audio: uploadRes.url,
                    audioSynced: true,
                    audioKey: uploadRes.data.Key,
                    audioFileName: audioPath.split('/').pop(),
                    audioUser: req.user._id,
                    audioProcessed: true,
                    audioSource: 'tts',
                };
                return articleService.updateSubslideUsingPosition(articleId, slidePosition, subslidePosition, articleUpdate)
        })
        .then((doc) => {
            res.json({ audio: uploadedAudioUrl, slidePosition, subslidePosition, audioSynced: true })
            fs.unlink(audioPath, (err) => {
                if (err) {
                    console.log('error dleting tts audio', err);
                }
            })
            websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.TRANSLATION_SUBSLIDE_CHANGE}/${articleId}`, { slidePosition, subslidePosition, changes: { audio: uploadedAudioUrl } });
            // Delete old audio in the slide
            const oldSubslide = article.slides.find(s => parseInt(s.position) === parseInt(slidePosition)).content.find(s => parseInt(s.position) === subslidePosition);
            if (oldSubslide.audioFileName) {
                storageService.deleteFile(TRANSLATION_AUDIO_DIRECTORY, oldSubslide.audioFileName)
                        .then(() => {
                            console.log('deleted file');
                        })
                        .catch((err) => {
                            console.log('error deleting file', err);
                        });
            }

            articleService.update({ _id: articleId }, { exported: false })
            .then(() => {

            })
            .catch(err => {
                console.log('error updating article exported', err);
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    updateVideoSpeed: function(req, res) {
        const { articleId } = req.params;
        const { videoSpeed } = req.body;
        articleService.findById(articleId)
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article id');
            let article = articleDoc.toObject();
            if (article.videoSpeed === videoSpeed) throw new Error(`speed is already ${videoSpeed}x`);
            if (article.videoSpeedLoading) throw new Error('Already processing');

            return articleService.updateById(articleId, { videoSpeedLoading: true })
        })
        .then(() => {
            exporterWorker.updateArticleVideoSpeed({ articleId, videoSpeed })
            return res.json({ success: true, videoSpeedLoading: true });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    }

}

module.exports = controller;
