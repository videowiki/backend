
const rabbitmqService = require('../shared/vendors/rabbitmq');
const queues = require('../shared/vendors/rabbitmq/queues');
const videoService = require('../shared/services/video');
const articleService = require('../shared/services/article');
const subtitlesService = require('../shared/services/subtitles');
const socketConnectionService = require('../shared/services/socketConnection');
const translationExportService = require('../shared/services/translationExport');
const noiseCancellationVideoService = require('../shared/services/noiseCancellationVideo');
const websockets = require('../shared/vendors/websockets');
const websocketsRooms = require('../shared/vendors/websockets/rooms');
const websocketsEvents = require('../shared/vendors/websockets/events');

const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
let rabbitmqChannel;

function init() {
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        rabbitmqChannel.prefetch(1);
        rabbitmqChannel.assertQueue(queues.PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, { durable: true });
        rabbitmqChannel.consume(queues.PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, onNoiseCancellationVideoFinish, { noAck: false });
    });
}

function onNoiseCancellationVideoFinish(msg) {
  const { noiseCancellationVideoId } = JSON.parse(msg.content.toString());
  rabbitmqChannel.ack(msg);
  let noiseCancellationVideo;
  noiseCancellationVideoService.findById(noiseCancellationVideoId)
  .then((doc) => {
    noiseCancellationVideo = doc.toObject();
    console.log('sending to clients ', onNoiseCancellationVideoFinish)
    websockets.ioEmitter.to(websocketsRooms.getOrganizationRoom(video.organization))
      .emit(websocketsEvents.NOISE_CANCELLATION_VIDEO_FINISH, { noiseCancellationVideo });
  })
  .catch(err => {
    console.log(err);
  })
}

module.exports = {
    init,
}
