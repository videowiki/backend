const async = require('async');
const rabbitmqService = require('../shared/vendors/rabbitmq');
const queues = require('../shared/vendors/rabbitmq/queues');
const videoService = require('../shared/services/video');
const articleService = require('../shared/services/article');
const socketConnectionService = require('../shared/services/socketConnection');
const translatioService = require('../shared/services/translation')
const websockets = require('../shared/vendors/websockets');
const websocketsRooms = require('../shared/vendors/websockets/rooms');
const websocketsEvents = require('../shared/vendors/websockets/events');

const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
let rabbitmqChannel;

function init() {
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        rabbitmqChannel.consume(queues.CONVERT_VIDEO_TO_ARTICLE_FINISH_QUEUE, onConvertArticleFinish, { noAck: false });
        rabbitmqChannel.consume(queues.VIDEO_PROOFREADING_READY, onProofreadReady, { noAck: false });
        rabbitmqChannel.consume(queues.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH_QUEUE, onExtractBackgroundMusicFinish, { noAck: false });
        

    });
}

function onConvertArticleFinish(msg) {
    // Archive all old article Translations
    const { articleId, videoId } = JSON.parse(msg.content.toString());

    let article;
    let video
    videoService.findById(videoId).then(videoDoc => {
        if (!videoDoc) throw new Error(`Invalid video id ${videoId}`);
        video = videoDoc.toObject();
        // Send notification to users that the video is now done
        // websockets.ioEmitter.to(websocketsRooms.getOrganizationRoom(video.organization))
        //     .emit(websocketsEvents.VIDEO_DONE, video);

        return articleService.findById(articleId)
    })
    .then((articleDoc) => {
        if (!articleDoc) throw new Error('Invalid article id');
        article = articleDoc.toObject();
        // If the article is just refreshing the media content, don't archive old articles
        if (article.refreshing) {
            console.log('just refreshing');
            return articleService.update({ _id: articleId }, { refreshing: false });
        } else {
            console.log('archiving all old articles');
            return articleService.update({ video: videoId, _id: { $nin: [articleId] } }, { archived: true })
        }
    })
    .then(() => socketConnectionService.findOne({ userId: video.convertedBy }))
    .then((socketConn) => {
        if (!socketConn) {
            console.log('user offline')
            return Promise.resolve();
        }
        websockets.ioEmitter.to(socketConn.socketId)
        .emit(websocketsEvents.VIDEO_DONE, video);
        return Promise.resolve()
    })
    .then((result) => {
        rabbitmqChannel.ack(msg)
        console.log('done afterconvert rabbitmq');
        if (article.refreshing && article.articleType === 'original') {
            // if it was refreshing, update other articles with the new medias
            return articleService.syncTranslationArticlesMediaWithOriginal(articleId);
        } else {
            // Generate TTS version for directly to english articles
            if (!article.toEnglish) return;
            return translatioService.generateTTSArticle(articleId, 'en')
        }
    })
    .catch((err) => {
        rabbitmqChannel.ack(msg)
        console.log(err);
    })
}

function onProofreadReady(msg) {
    const { videoId } = JSON.parse(msg.content.toString());

    videoService.findById(videoId)
        .then((video) => {
            if (!video) throw new Error(`Invalid video id ${videoId}`);
            rabbitmqChannel.ack(msg);
            websockets.ioEmitter.to(websocketsRooms.getOrganizationRoom(video.organization))
                .emit(websocketsEvents.VIDEO_TRANSCRIBED, video.toObject());
        })
        .catch((err) => {
            console.log(err);
            rabbitmqChannel.ack(msg);
        })
}

function onExtractBackgroundMusicFinish(msg) {
    const { videoId } = JSON.parse(msg.content.toString());
    console.log('extracting background music finished', videoId)
    let video;
    videoService.findById(videoId)
        .then((videoDoc) => {
            if (!videoDoc) throw new Error(`Invalid video id ${videoId}`);
            video = videoDoc.toObject();
            return videoService.update({ _id: videoId }, { backgroundMusicTransposed: true, hasBackgroundMusic: true })
        })
        .then(() => {
            rabbitmqChannel.ack(msg);
            websockets.ioEmitter.to(websocketsRooms.getOrganizationRoom(video.organization))
                .emit(websocketsEvents.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH, video);
        })
        .catch((err) => {
            console.log(err);
            rabbitmqChannel.ack(msg);
        })
}

module.exports = {
    init,
}