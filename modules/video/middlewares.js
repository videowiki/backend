const fs = require('fs');
const videoService = require('../shared/services/video');
const { handlePromiseReject } = require('../shared/utils/helpers')
const middlewares = {
    authorizeAdminAndReviewer: async function(req, res, next) {
        const { err, data: video } = await handlePromiseReject(videoService.findById(req.params.id));
        if (err) return res.status(400).send(err.message);
        if (!video) return res.status(400).send('invalid video id');
        const userRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === video.organization.toString());
        if (userRole && (userRole.organizationOwner || (userRole.permissions && userRole.permissions.indexOf('admin') !== -1))) return next();
        if (!userRole || (!userRole.organizationOwner && userRole.permissions.indexOf('admin') === -1 && userRole.permissions.indexOf('review') === -1)) return res.status(401).send('Unauthorized');
        // If there's no reviewers, allow user if a reviewer to export
        if ((!video.reviewers || video.reviewers.length === 0) && userRole && userRole.permissions.indexOf('review') !== -1) return next();
        // Authorize user's one of the assigned reviewers
        if (video.reviewers.map(r => r.toString()).indexOf(req.user._id.toString()) === -1 &&
                (video.verifiers || []).map(r => r.toString()).indexOf(req.user._id.toString()) === -1 )
            return res.status(401).send("You're not assigned to review this video");
        return next();
    },
    authorizeUploadVideo: async function(req, res, next) {
        const { organization } = req.body;
        const userRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === organization);
        if (!userRole || (!userRole.organizationOwner && userRole.permissions.indexOf('admin') === -1)) {
            if (req.file && req.file.path) {
                fs.unlink(req.file.path, () => {})
            }
            return res.status(401).send('Unauthorized');
        }
        return next();
    },
    authorizeVideoAdmin: function(req, res, next) {
        const { id } = req.params;
        videoService.findById(id)
        .then((video) => {
            const userRole = req.user.organizationRoles.find((role) => role.organization._id.toString() === video.organization.toString());
            if (!userRole || (!userRole.organizationOwner && userRole.permissions.indexOf('admin') === -1)) {
                return res.status(401).send('Unauthorized');
            }
            return next();
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    }
}

module.exports = middlewares;