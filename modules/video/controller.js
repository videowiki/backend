const fs = require('fs');
const async = require('async');
const storageVendor = require('../shared/vendors/storage');
const videoService = require('../shared/services/video');
const articleService = require('../shared/services/article');
const exporterWorker = require('../shared/workers/exporter');
const transcriberWorker = require('../shared/workers/transcriber');
const spleeterWorker = require('../shared/workers/spleeter');
const userService = require('../shared/services/user');
const authService = require('../shared/services/auth');
const emailService = require('../shared/services/email');
const websockets = require('../shared/vendors/websockets');
const websocketsEvents = require('../shared/vendors/websockets/events');
const websocketsRooms = require('../shared/vendors/websockets/rooms'); 
const { supportedTranscribeLangs, BACKGROUND_MUSIC_DIRECTORY } = require('../shared/constants');

const fileUtils = require('../shared/utils/fileUtils');

const SILENCE_THREASHOLD = 0.1; // silence threashold in seconds

// setTimeout(() => {
//     videoService.find({ thumbnailUrl: { $exists: false }})
//     .then((videos) => {
//         videos.forEach((video) => {
//             console.log('generating thumbnail for', video._id);
//             exporterWorker.generateVideoThumbnail(video._id)
//         })
//     })
//     .catch(err => {
//         console.log(err);
//     })
// }, 5000);

const transcribeVideosQueue = () => async.queue(({ skip, limit, organization }, callback) => {
    console.log(skip, limit);
    videoService.find({ organization, status: 'uploaded' })
    .skip(skip)
    .limit(limit)
    .then((videos) => {
        if (videos.length === 0) {
            return callback();
        }
        const reviewVideoFuncArray = [];
        videos.forEach((video) => {
            reviewVideoFuncArray.push((cb) => {
                // If it's a supported transcribe lang, transcribe it
                // otherwise, create a dummy article and let the user fill in the text
                if (supportedTranscribeLangs.map(l => l.code).indexOf(video.langCode) !== -1) {
                    transcriberWorker.transcribeVideo(video._id)
                    videoService.update({ _id: video._id }, { status: 'transcriping' })
                    .then(() => {
                        cb()
                    })
                    .catch(err => {
                        console.log(err);
                        cb()
                    })
                } else {

                    const initialSlide = {
                        position: 0,
                        content: [{
                            text: '',
                            position: 0,
                            startTime: 0,
                            endTime: video && video.duration ? video.duration : 1,
                            speakerProfile: {
                                speakerNumber: 1,
                                speakerGender: 'male',
                            }
                        }]
                    }
                    const newArticle = {
                        title: video.title,
                        version: 1,
                        slides: [initialSlide],
                        video: video._id,
                        numberOfSpeakers: video.numberOfSpeakers,
                        langCode: video.langCode,
                        speakersProfile: [{
                            speakerNumber: 1,
                            speakerGender: 'male',
                        }],
                        organization: video.organization,
                        archived: false,
                    }
                    articleService.createArticle(newArticle)
                    .then((newArticle) => {
                        return videoService.update({ _id: video._id }, { status: 'proofreading', article: newArticle._id });
                    })
                    .then(() => {
                        cb()
                    })
                    .catch((err) => {
                        console.log(err);
                        cb()
                    })
                }
            })
        })
        async.parallelLimit(reviewVideoFuncArray, 10, (err) => {
            console.log(err);
            return callback();
        })
    })
    .catch(err => {
        console.log(err);
        return callback(err);
    })
})
const controller = {
    uploadVideo: function (req, res) {
        // 1- Create new video instance
        // 2- Upload file to s3
        // 3- Send to the exporter to transcribe it
        const { title, numberOfSpeakers, langCode, organization, url } = req.body;
        let file = req.files &&  req.files.find((f) => f.fieldname === 'video');
        let subtitle;
        let backgroundMusic;
        if (req.files && req.files.find((f) => f.fieldname === 'subtitle')) {
            subtitle = req.files.find((f) => f.fieldname === 'subtitle');
        }
        if (req.files && req.files.find((f) => f.fieldname === 'backgroundMusic')) {
            backgroundMusic = req.files.find((f) => f.fieldname === 'backgroundMusic');
        }
        let uploadFilePromise;
        if (file) {
            uploadFilePromise = storageVendor.saveFile('videos', file.filename, fs.createReadStream(file.path))
        } else if (url) {
            uploadFilePromise = new Promise((resolve, reject) => {
                fileUtils.downloadFile(url)
                .then((filePath) => {
                    newFilePath = filePath;
                    file = { path: newFilePath };
                    return storageVendor.saveFile('videos', filePath.split('/').pop(), fs.createReadStream(filePath))
                })
                .then((data) => {
                    return resolve(data);
                })
                .catch(reject)
            })
        } else {
            return res.status(400).send('Please upload video file or a video url')
        }
        // if (process.env.NODE_ENV === 'production') {
        //     console.log('======================= Uploading to s3 ======================== ');
        //     uploadFilePromise = storageVendor.saveFile('videos', file.filename, fs.createReadStream(file.path))
        // } else {
        //     uploadFilePromise = new Promise((resolve) => resolve({url: file.path, data: { Key: file.filename } }));
        // }
        let video;
        const videoData = {
            title,
            status: 'uploading',
            numberOfSpeakers,
            langCode,
            uploadedBy: req.user._id,
        }
        if (subtitle) {
            videoData.withSubtitle = true;
        }
        if (organization) {
            videoData.organization = organization;
        }
        console.log('before upload')
        videoService.createVideo(videoData)
        .then((doc) => {
            video = doc.toObject();
            return uploadFilePromise
        })
        .then((result) => {
            console.log(' =============== uploaded ====================');
            fs.unlink(file.path, () => { });
            const { url, data } = result;
            const Key = data.Key;
            video.Key = Key;
            video.url = url;
            video.status = 'uploaded';
            res.json(video);
            console.log('created video', video)
            return videoService.update({ _id: video._id }, { Key, url, status: 'uploaded' })
        })
        .then(() => fileUtils.getFileDuration(video.url))
        .then((videoDuration) => videoService.update({ _id: video._id }, { duration: videoDuration }))
        .then((doc) => {
            console.log('uploaded doc', doc)
            // websockets.ioEmitter.to(websocketsRooms.getOrganizationRoom(video.organization)).emit(websocketsEvents.VIDEO_UPLOADED, video);

            // Generate thumbnail image
            exporterWorker.generateVideoThumbnail(video._id);
            // Upload subtitle
            if (subtitle) {
                storageVendor.saveFile('subtitles', subtitle.filename, fs.createReadStream(subtitle.path))
                .then((result) => {
                    const { url } = result;
                    video.subtitle = url;
                    return videoService.update({ _id: video._id }, { subtitle: url, status: 'cutting' });
                })
                .then(() => {
                    transcriberWorker.transcribeVideo(video._id);
                    fs.unlink(subtitle.path, () => { });

                })
                .catch((err) => {
                    console.log(err);
                    videoService.update({ _id: video._id }, { withSubtitle: false });  
                })
            }
            // Upload backgroundMusic
            if (backgroundMusic) {
                console.log('uploading background music', backgroundMusic);
                storageVendor.saveFile('backgroundMusic', backgroundMusic.filename, fs.createReadStream(backgroundMusic.path))
                .then((result) => {
                    // backgroundMusicUrl
                    // backgroundMusicKey
                    const { url } = result;
                    video.backgroundMusicUrl = url;
                    return videoService.update({ _id: video._id }, { backgroundMusicUrl: url, backgroundMusicKey: result.data.Key });
                })
                .then(() => {
                    console.log('uploaded bg music')
                    fs.unlink(backgroundMusic.path, () => { });
                })
                .catch((err) => {
                    console.log('error uploading background music', err);
                })
            }
        })
        .catch(err => {
            console.log(err);
            fs.unlink(file.path, () => { });
            videoService.update({ _id: video._id }, { status: 'failed' });
        })
    },

    updateVideo: function(req, res) {
        const { id } = req.params;
        const changes = req.body;
        console.log('changes are', req.body)
        let subtitle;
        let backgroundMusic;
        if (req.files && req.files.find((f) => f.fieldname === 'subtitle')) {
            subtitle = req.files.find((f) => f.fieldname === 'subtitle');
        }
        if (req.files && req.files.find((f) => f.fieldname === 'backgroundMusic')) {
            backgroundMusic = req.files.find((f) => f.fieldname === 'backgroundMusic');
        }
        if (changes.backgroundMusic === '') {
            changes.backgroundMusicUrl = '';
            delete changes.backgroundMusic;
        }
        let video;
        let archiveArticles = false;
        videoService.findById(id)
        .then((videoDoc) => {
            if (!videoDoc) throw new Error('Invalid video id');
            video = videoDoc.toObject();
            if (!changes || Object.keys(changes).length === 0) return Promise.resolve();
            
            // If the langCode changed, put the video back to uploaded state
            if (changes.langCode && changes.langCode !== video.langCode) {
                changes.status = 'uploaded';
                changes.jobName = '';
                changes.transcriptionUrl = '';
                changes.transcripingProgress = 0;
                changes.article = null;
                archiveArticles = true;
            }
            return videoService.update({ _id: id }, changes);
        })
        .then(() => {
            // Subtitles upload if exists
            return new Promise((resolve, reject) => {
                if (subtitle) {
                    storageVendor.saveFile('subtitles', subtitle.filename, fs.createReadStream(subtitle.path))
                    .then((result) => {
                        const { url } = result;
                        video.subtitle = url;
                        return videoService.update({ _id: video._id }, { subtitle: url, status: 'cutting' });
                    })
                    .then(() => {
                        transcriberWorker.transcribeVideo(video._id);
                        fs.unlink(subtitle.path, () => { });
                        resolve();
                    })
                    .catch((err) => {
                        videoService.update({ _id: video._id }, { withSubtitle: false });
                        reject(err);
                    })
                } else {
                    return resolve();
                }
            })
        })
        .then(() => {
            return new Promise((resolve, reject) => {
                // Background music upload if exists
                // Upload backgroundMusic
                if (backgroundMusic) {
                    storageVendor.saveFile('backgroundMusic', backgroundMusic.filename, fs.createReadStream(backgroundMusic.path))
                    .then((result) => {
                        // backgroundMusicUrl
                        // backgroundMusicKey
                        const { url } = result;
                        video.backgroundMusicUrl = url;
                        return videoService.update({ _id: video._id }, { backgroundMusicUrl: url, backgroundMusicKey: result.data.Key });
                    })
                    .then(() => {
                        fs.unlink(backgroundMusic.path, () => { });
                        resolve();
                    })
                    .catch((err) => {
                        reject();
                    })
                } else {
                    return resolve();
                }
            })
        })
        .then(() => videoService.findById(id))
        .then((videoDoc) => {
            // If the title or number of speaker changed, map changes to all related articles
            if (changes.title || changes.numberOfSpeakers) {
                const articleChages = {};
                if (changes.title) {
                    articleChages.title = changes.title;
                }
                if (changes.numberOfSpeakers) {
                    articleChages.numberOfSpeakers = changes.numberOfSpeakers;
                }
                articleService.update({ video: id }, articleChages, { multi: true })
                .then((r) => {
                    console.log('update article due to video change', articleChages, r);
                })
                .catch(err => {
                    console.log('error update article due to video change', err);
                })
            }
            return res.json({ video: videoDoc.toObject() });
        })
        .then(() => {
            // Archive old articles if langCode changed, look condition above
            if (archiveArticles) {
                articleService.update({ video: id }, { archived: true })
                .then((r) => {
                    console.log('archived articles', r);
                })
                .catch(err => {
                    console.log('error archiving articles', err);
                })
            }
        })
        .catch((err) => {
            return res.status(400).send(err.message);
        })
    },

    uploadBackgroundMusic: function(req, res) {
        const file = req.files.find((f) => f.fieldname === 'file');
        const { id } = req.params;
        uploadFilePromise = storageVendor.saveFile('videos', file.filename, fs.createReadStream(file.path))
        let video;
        let newBackgroundMusicUrl;
        videoService.findById(id)
        .then((videoDoc) => {
            if (!videoDoc) throw new Error('Invalid video id');
            video = videoDoc.toObject();
            return storageVendor.saveFile(BACKGROUND_MUSIC_DIRECTORY, file.filename, fs.createReadStream(file.path))
        })
        .then((uploadRes) => {
            console.log(uploadRes);
            // Delete old file if exiists
            if (video.backgroundMusicKey) {
                storageVendor.deleteFile(video.backgroundMusicKey)
                .then((d) => {
                })
                .catch(err => {
                    console.log('error deleting file', err);
                })
            }
            newBackgroundMusicUrl = uploadRes.url;
            const videoUpdate = {
                backgroundMusicUrl: uploadRes.url,
                backgroundMusicKey: uploadRes.data.Key,
                backgroundMusicTransposed: false,
                hasBackgroundMusic: true,
            }
            return videoService.update({ _id: id }, videoUpdate);
        })
        .then(() => {
            fs.unlink(file.path, (err) => {
                if (err) {
                    console.log('error removing tmp file', err);
                }
            })
            video.backgroundMusicUrl = newBackgroundMusicUrl;
            return res.json({ video })
        })
        .catch(err => {
            console.log(err);
        })
    },

    deleteBackgroundMusic: function(req, res) {
        const { id } = req.params;
        let video;
        videoService.findById(id)
        .then((videoDoc) => {
            video = videoDoc.toObject();
            if (!video.backgroundMusicKey) throw new Error('The video doesnt have background music');
            return storageVendor.deleteFile(video.backgroundMusicKey)
        })
        .then(() => {
            return videoService.update({ _id: id }, { backgroundMusicKey: '', backgroundMusicUrl: '', backgroundMusicTransposed: false });
        })
        .then(() => videoService.findById(id))
        .then(video => {
            return res.json({ video: video.toObject() });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    extractVideoBackgroundMusic: function(req, res) {
        const { id } = req.params;
        let video;
        videoService.findById(id)
        .then((videoDoc) => {
            video = videoDoc.toObject();
            // if (video.extractBackgroundMusicLoading) throw new Error('Extracting background music already in progress');
            return spleeterWorker.extractVideoBackgroundMusic(id);
        })
        .then(() => {
            return videoService.update({ _id: id }, { extractBackgroundMusicLoading: true });
        })
        .then(() => videoService.findById(id))
        .then(video => {
            return res.json({ video: video.toObject() });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
    

    updateReviewers: function(req, res) {
        const { id } = req.params;
        const { reviewers } = req.body;
        const newReviewers = [];
        let video;
        videoService.findById(id)
        .populate('organization')
        .then(videoDoc => {
            video = videoDoc.toObject()
            if (reviewers && reviewers.length > 0) {
                const oldReviewers = video.reviewers.map(r => r.toString());
                reviewers.forEach((reviewer) => {
                    if (oldReviewers.indexOf(reviewer) === -1) {
                        newReviewers.push(reviewer);
                    }
                })
            }
            return videoService.update({ _id: id }, { reviewers })
        })
        .then(() => {
            return res.json({ reviewers });
        })
        .then(() => {
            if (newReviewers.length > 0) {
                newReviewers.forEach(reviewer => {
                    let user;
                    userService.findById(reviewer)
                    .then((userDoc) => {
                        user = userDoc.toObject();
                       return authService.generateLoginToken(user.email);
                    })
                    .then((token) => {
                        return emailService.inviteUserToReview({
                            from: req.user,
                            to: user,
                            videoId: id,
                            organizationName: video.organization.name,
                            organizationId: video.organization._id,
                            videoTitle: video.title,
                            inviteToken: token,
                        })
                    })
                    .catch((err) => {
                        console.log('Error sending email to user', err);
                    })
                });
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    updateVerifiers: function(req, res) {
        const { id } = req.params;
        const { verifiers } = req.body;
        const newVerifiers = [];
        console.log(verifiers)
        let video;
        videoService.findById(id)
        .populate('organization')
        .then(videoDoc => {
            video = videoDoc.toObject()
            if (verifiers && verifiers.length > 0) {
                console.log(video.verifiers)
                const oldVerifiers = video.verifiers.map(r => r && r.toString());
                verifiers.forEach((reviewer) => {
                    if (oldVerifiers.indexOf(reviewer) === -1) {
                        newVerifiers.push(reviewer);
                    }
                })
            }
            return videoService.update({ _id: id }, { verifiers })
        })
        .then(() => {
            return res.json({ verifiers });
        })
        .then(() => {
            if (newVerifiers.length > 0) {
                newVerifiers.forEach(reviewer => {
                    let user;
                    userService.findById(reviewer)
                    .then((userDoc) => {
                        user = userDoc.toObject();
                       return authService.generateLoginToken(user.email);
                    })
                    .then((token) => {
                        return emailService.inviteUserToVerifyVideo({
                            from: req.user,
                            to: user,
                            videoId: id,
                            organizationName: video.organization.name,
                            organizationId: video.organization._id,
                            videoTitle: video.title,
                            inviteToken: token,
                        })
                    })
                    .catch((err) => {
                        console.log('Error sending email to user', err);
                    })
                });
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    getVideos: function(req, res) {
        const perPage = 10;
        let { organization, page, search } = req.query;

        const query = {
            organization: organization,
        }
        const queryKeys = Object.keys(req.query)
        // Remove page if it's in the query
        if (queryKeys.indexOf('page') !== -1) {
            delete req.query.page
        }
        
        if (queryKeys.indexOf('search') !== -1) {
            query.title = new RegExp(search, 'ig');
            delete req.query.search;
        }

        if (page) {
            page = parseInt(page);
        } else {
            page = 1;
        }
        const skip = page === 1 || page === 0 ? 0 : (page * perPage - perPage);

        Object.keys(req.query).forEach(key => {
            if (req.query[key]) {
                query[key] = req.query[key];
            }
        });
        // Status field is a special case as it's an array
        if (req.query['status']) {
            const statusList = req.query.status.split(',');
            query.status = {$in: statusList}
        }
        let videos;
        videoService.find(query)
        .populate('reviewers')
        .populate('verifiers')
        .populate('uploadedBy')
        .skip(skip)
        .limit(perPage)
        .sort({ created_at: -1 })
        .then((v) => {
            videos = v;
            return videoService.count(query);
        })
        .then((count) => {
            return res.json({ videos, pagesCount: Math.ceil(count/perPage), totalCount: count });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },

    getVideosCount: function(req, res) {
        const { organization } = req.query;
        let transcribe = 0, proofread = 0, completed = 0;
        // Transcribe videos
        videoService.count({ organization, status: {$in: ['uploaded', 'transcriping']} })
        .then((count) => {
            transcribe = count;
            return videoService.count({ organization, status: 'proofreading' })
        })
        .then((count) => {
            proofread = count;
            return videoService.count({ organization, status: 'done' })
        })
        .then((count) => {
            completed = count;
            return res.json({ transcribe, proofread, completed, total: transcribe + proofread + completed });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },

    getVideoById: function (req, res) {
        const { id } = req.params;
        videoService.findById(id)
            .populate('reviewers')
            .populate('verifiers')
            .then(video => {
                return res.json(video);
            })
            .catch(err => {
                return res.status(400).send('Something went wrong');
            })
    },

    deleteVideo: function(req, res) {
        const { id } = req.params;
        let video;
        videoService.findById(id)
            .then((videoDoc) => {
                video = videoDoc.toObject();
                return videoService.remove({ _id: id })
            })
            .then(video => {
                // videoService.deleteVideosMedia([video]);
                return articleService.find({ video: id })
            })
            .then((articles) => {
                // articleService.deleteArticlesMedia(articles);
                return articleService.remove({ video: id })
            })
            .then(() => {
                return res.json({ success: true });
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send('Something went wrong');
            })
    },

    transcribeAllVideos: function(req, res) {
        const { organization } = req.body;
        const transcribeQueue = transcribeVideosQueue();
       console.log('organization is', organization)
        videoService.count({ organization, status: 'uploaded' })
        .then((count) => {
            if (count === 0) {
                throw new Error('No videos to be transcribed');
            }
            const limitPerOperation = 1;
            for (let i = 0; i < count; i += limitPerOperation) {
                transcribeQueue.push({ skip: 0, limit: limitPerOperation, organization });
            }
            transcribeQueue.drain(function() {
                return res.json({ success: true, queued: true });
            })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
        
    },

    transcribeVideo: function(req, res) {
        const { id } = req.params;
        let video;
        let article;
        let newClonedArticle;
        videoService.findById(id)
        .then(videoDoc => {
            if (!videoDoc) {
                throw new Error('Invalid video id');
            }
            video = videoDoc;
            return articleService.find({_id: video.article})
        })
        .then((articleDocs) => {
            // If there's already existing articles
            // Then the user is requesting a re-review
            if (articleDocs && articleDocs.length > 0) {
                article = articleDocs[0].toObject();
                console.log('article', article)
                // If the article was already converted, clone the converted article for a new version of review
                if (article && article.converted) {
                    return articleService.cloneArticle(article._id)
                    .then((clonedArticle) => {
                        const lastVersion = article.version;
                        newClonedArticle = articleService.cleanArticleSilentSlides(clonedArticle);
                        return articleService.update({ _id: newClonedArticle._id }, { slides: newClonedArticle.slides, version: lastVersion + 1, converted: false , archived: false })
                    })
                    .then(() => {
                        return videoService.update({ _id: id }, { status: 'proofreading', article: newClonedArticle._id });
                    })
                    .then(() => {
                        return res.json({ success: true, article: newClonedArticle })
                    })
                    .catch((err) => {
                        console.log(err);
                        return res.status(400).send(err.message);
                    })
                } else {
                    console.log('not cloning')
                    return videoService.update({ _id: id }, { status: 'proofreading' }).then(() => {
                        return res.json({ success: true, article: articleDocs[0] });

                    })
                    .catch((err) => {
                        console.log(err);
                        return res.status(400).send('Something went wrong')
                    })

                }
            }
            if (video.jobName) {
                return res.json({ success: true, queued: true });
            }
            // If it's a supported transcribe lang, transcribe it
            // otherwise, create a dummy article and let the user fill in the text
            if (supportedTranscribeLangs.map(l => l.code).indexOf(video.langCode) !== -1) {
                transcriberWorker.transcribeVideo(video._id)
                videoService.update({ _id: video._id }, { status: 'transcriping' })
                .then(() => {
                    return res.json({ success: true, queued: true });
                })
                .catch(err => {
                    console.log(err);
                    return res.json({ success: true, queued: true });
                })
            } else {
                const initialSlide = {
                    position: 0,
                    content: [{
                        text: '',
                        position: 0,
                        startTime: 0,
                        endTime: video && video.duration ? video.duration : 1,
                        speakerProfile: {
                            speakerNumber: 1,
                            speakerGender: 'male',
                        }
                    }]
                }
                const newArticle = {
                    title: video.title,
                    version: 1,
                    slides: [initialSlide],
                    video: video._id,
                    numberOfSpeakers: video.numberOfSpeakers,
                    langCode: video.langCode,
                    speakersProfile: [{
                        speakerNumber: 1,
                        speakerGender: 'male',
                    }],
                    organization: video.organization,
                    archived: false,
                }
                articleService.createArticle(newArticle)
                .then((article) => {
                    return videoService.update({ _id: video._id }, { status: 'proofreading', article: article._id });
                })
                .then(() => {
                    return res.json({ success: true, article: newArticle });
                })
                .catch((err) => {
                    throw err;
                })
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    skipTranscribe: function(req, res) {
        const { id } = req.params;
        let video;
        videoService.findById(id)
        .then((videoDoc) => {
            if (!videoDoc) throw new Error('Invalid video id');
            video = videoDoc.toObject();
            return fileUtils.getFileDuration(video.url)
        })
        .then((videoDuration) => {

            const initialSlide = {
                position: 0,
                content: [{
                    text: '',
                    position: 0,
                    startTime: 0,
                    endTime: videoDuration,
                    speakerProfile: {
                        speakerNumber: 1,
                        speakerGender: 'male',
                    }
                }]
            }
            const newArticle = {
                title: video.title,
                version: 1,
                slides: [initialSlide],
                video: video._id,
                numberOfSpeakers: video.numberOfSpeakers,
                langCode: video.langCode,
                speakersProfile: [{
                    speakerNumber: 1,
                    speakerGender: 'male',
                }],
                organization: video.organization,
            }
            articleService.createArticle(newArticle)
            .then((newArticle) => {
                return videoService.update({ _id: video._id }, { status: 'proofreading', article: newArticle._id });
            })
            .then(() => {
                return res.json({ success: true, article: newArticle });
            })
            .catch((err) => {
                throw err;
            })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    refreshMedia: function(req, res) {
        const { id } = req.params;
        const { articleId } = req.body;
        articleService.find({ _id: articleId, video: id })
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Invalid article/video id');
            return articleService.update({ _id: articleId }, { refreshing: true, toEnglish: false });
        })
        .then(() => {
            console.log('article id is', articleId)
            exporterWorker.convertVideoToArticle({ videoId: id, articleId });
            return res.json({ refreshing: true });
        })
        .catch((err) => {
            console.log('error refreshing media', err);
            return res.status(400).send(err.message);
        })
    },

    convertVideo: function(req, res) {
        const { id } = req.params;
        const { articleId } = req.body;
        console.log(id, articleId)
        let video;
        let article;
        videoService.findById(id)
        .then((v) => {
            if (!v) throw new Error('Invalid video id');
            video = v;
            if (video.status !== 'proofreading') {
                throw new Error('Someone is already converting this video');
            }
            return articleService.find({ video: video._id, _id: articleId });
        })
        .then(a => {
            if (!a || a.length === 0) throw new Error('Invalid article');
            // Sort the article's slides/subslides based on startTime before sending to the exporter
            article = a[0].toObject();
            const { slides } = article;
            slides.forEach(slide => {
                slide.content = slide.content.sort((a, b) => a.startTime - b.startTime);
            });
            // merge slides content
            const subslides = slides.reduce((acc, slide) => acc.concat(slide.content), []).sort((a,b) => a.startTime - b.startTime);
            // Find the silent parts and cut into separate sub-slides
            let newSubslides = [];
            /*
                this part handles gaps between slides
            */
            // subslides.forEach((subslide, index) => {
            //     // Handle intro part
            //     if (index === 0 && subslide.startTime !== 0 && subslide.startTime > SILENCE_THREASHOLD) {
            //         newSubslides.push({
            //             startTime: 0,
            //             endTime: subslide.startTime,
            //             text: '',
            //             silent: true,
            //         })
            //     }
            //     newSubslides.push(subslide);
            //     if (index !== subslides.length - 1) {
            //         // compare the current and prev slide startTime/endTime
            //         // if they don't match, add as a new subslide
            //         if (subslide.endTime <= subslides[index + 1].startTime && (subslides[index + 1].startTime - subslide.endTime) > SILENCE_THREASHOLD) {
            //             console.log('silence', subslides[index + 1].startTime - subslide.endTime)
            //             newSubslides.push({
            //                 startTime: subslide.endTime,
            //                 endTime: subslides[index + 1].startTime,
            //                 text: '',
            //                 silent: true,
            //             })
            //             console.log({
            //                 startTime: subslide.endTime,
            //                 endTime: subslides[index + 1].startTime,
            //                 text: '',
            //                 silent: true,
            //             })
            //         }
            //     } else {
            //         // Handle outro part
            //         if (video.duration && subslide.endTime < video.duration &&  (video.duration - subslide.endTime) > SILENCE_THREASHOLD) {
            //             newSubslides.push({
            //                 startTime: subslide.endTime,
            //                 endTime: video.duration,
            //                 text: '',
            //                 silent: true,
            //             })
            //         }
            //     }
            // })
            /*
                Add gaps between slides to previous slide
            */
            subslides.forEach((subslide, index) => {
                // First slide starts at 0
                if (index === 0) {
                    subslide.startTime = 0;
                }
                // Last slide, set its end time to the video duration
                if (index === subslides.length - 1) {
                    subslide.endTime = video.duration;
                } else {
                    
                    subslide.endTime = subslides[index + 1].startTime;
                }
                newSubslides.push(subslide)
            })
            newSubslides = newSubslides.filter((s) => (s.endTime - s.startTime) > SILENCE_THREASHOLD);
            // re-cut subslides to slides based on 10 seconds rule
            const newSlides = [];
            let newSlide = {
                content: [newSubslides[0]],
            };
            for( let i = 1; i < newSubslides.length; i++) {
                if (newSlide.content.reduce((acc, s) => acc + (s.endTime - s.startTime), 0) >= 10) {
                    newSlides.push(newSlide);
                    newSlide = {
                        content: [],
                    }
                }
                newSlide.content.push(newSubslides[i]);
            }

            if (newSlide.content.length > 0) {
                newSlides.push(newSlide)
            }

            const orderedSlides = newSlides.map((s, index) => ({ ...s, position: index }));

            const articleUpdate = {
                slides: orderedSlides,
            }
            return articleService.update({ _id: article._id }, articleUpdate);
        })
        .then(() => {
            return videoService.update({ _id: id }, { status: 'converting', convertedBy: req.user._id });
        })
        .then(() => {
            return exporterWorker.convertVideoToArticle({ videoId: id, articleId });
        })
        .then(() => {
            return res.json({ queued: true });
        })
        .catch(err => {
            const reason = err.message || 'Somthing went wrong';
            console.log(err);
            return res.status(400).send(reason);
        })
    }

}

module.exports = controller;