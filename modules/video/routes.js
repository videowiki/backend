const path = require('path');
const controller = require('./controller');
const validation = require('./validate_request');
const middlewares = require('./middlewares');

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})

var upload = multer({ storage: storage })
// external modules should call the mount function and pass it an instance 
// of the router to add the module's routes to it
const mount = function (router) {
    // Define module routes here
    router.get('/', controller.getVideos)
    router.get('/count', controller.getVideosCount)
    
    router.post('/upload', upload.any(), middlewares.authorizeUploadVideo, validation.create_video, controller.uploadVideo)
    
    router.patch('/:id/backgroundMusic', upload.any(), middlewares.authorizeUploadVideo, controller.uploadBackgroundMusic)
    router.post('/:id/backgroundMusic/extract', middlewares.authorizeVideoAdmin, controller.extractVideoBackgroundMusic)
    router.delete('/:id/backgroundMusic', middlewares.authorizeVideoAdmin, controller.deleteBackgroundMusic)
    
    
    router.post('/:id/convert', middlewares.authorizeAdminAndReviewer, controller.convertVideo);
    router.post('/:id/refreshMedia', middlewares.authorizeAdminAndReviewer, controller.refreshMedia);
    
    router.post('/all/transcribe', controller.transcribeAllVideos);
    router.post('/:id/transcribe', middlewares.authorizeAdminAndReviewer, controller.transcribeVideo)
    router.post('/:id/transcribe/skip', middlewares.authorizeAdminAndReviewer, controller.skipTranscribe)
    
    router.put('/:id/reviewers', middlewares.authorizeVideoAdmin, controller.updateReviewers)
    router.put('/:id/verifiers', middlewares.authorizeVideoAdmin, controller.updateVerifiers)

    router.get('/:id', controller.getVideoById);
    router.patch('/:id', upload.any(), middlewares.authorizeVideoAdmin, controller.updateVideo);
    router.delete('/:id', middlewares.authorizeVideoAdmin, controller.deleteVideo)

    return router;
}

module.exports = {
    mount,
}
