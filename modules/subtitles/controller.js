const subtitlesService = require('../shared/services/subtitles');
const articleService = require('../shared/services/article');
const translationExportService = require('../shared/services/translationExport');
const exporterWorker = require('../shared/workers/exporter');

const controller = {
    getById: function(req, res) {
        const { id } = req.params;
        subtitlesService.findById(id)
        .then((subtitles) => {
            return res.json({ subtitles });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    resetSubtitles: function(req, res) {
        const { id } = req.params;
        let article;
        let subtitles;
        subtitlesService.findById(id)
        .then((subtitlesDoc) => {
            if (!subtitlesDoc) throw new Error('Invalid id');
            subtitles = subtitlesDoc.toObject();
            return articleService.findById(subtitles.article)
        })
        .then((articleDoc) => {
            if (!articleDoc) throw new Error('Article doesnt exists');
            article = articleDoc.toObject();

            subtitles.subtitles = subtitlesService.generateSubtitlesFromSlides(article.slides);
            return subtitlesService.updateById(id, { subtitles: subtitles.subtitles, updated_at: Date.now() });
        })
        .then(() => subtitlesService.findById(id))
        .then((subtitles) => res.json({ subtitles }))
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })      
    },

    getByArticleId: function(req, res) {
        const { id } = req.params;
        let translationExport;
        let subtitles;
        let article;
        translationExportService.find({ article: id, status: 'done' })
        .sort({ created_at: -1 })
        .limit(1)
        .then((translationExports) => {
            if (translationExports && translationExports[0]) {
                translationExport = translationExports[0].toObject();
            }
            return subtitlesService.findOne({ article: id })
                .populate('video')
        })
        .then((subtitlesDoc) => {
            if (subtitlesDoc) {
                subtitles = subtitlesDoc.toObject();
            }
            if (subtitles && translationExport) {
                return res.json({ subtitles, translationExport });
            }
            if (subtitles) return res.json({ subtitles });
            return articleService.findById(id)
                .populate('originalArticle')
                .then((articleDoc) => {
                    article = articleDoc.toObject();
                    if ((article.originalArticle.langCode.indexOf(article.langCode) !== 0) || article.tts ) {
                        return res.json({ locked: true });
                    }
                    // No subtitles but same language, generate a new subtitles doc
                    const subtitles = subtitlesService.generateSubtitlesFromSlides(article.slides)
                    const newSubtitles = {
                        article: article._id,
                        organization: article.organization,
                        video: article.video,
                        subtitles,
                    }
                    return subtitlesService.create(newSubtitles)
                        .then(() => subtitlesService.findOne({ article: article._id }))
                        .then((subtitles) => {
                            console.log('new subtitles is', subtitles.toObject())
                            return res.json({ subtitles: subtitles.toObject() });
                        })
                })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    splitSubtitle: function(req, res) {
        const { id, subtitlePosition } = req.params;
        const { wordIndex, time } = req.body;
        subtitlesService.splitSubtitle(id, parseInt(subtitlePosition), wordIndex, time)
        .then(() => subtitlesService.findById(id))
        .then((subtitles) => {
            return res.json({ subtitles })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    combineSubtitle: function(req, res) {
        const { id } = req.params;
        let { positions } = req.body;

        if (!positions || positions.length < 2 ) return res.status(400).send('There must be at least 2 positions to combine');
        
        positions = positions.map(p => parseInt(p));

        subtitlesService.combineSubtitles(id, positions)
        .then(() => subtitlesService.findById(id))
        .then((subtitles) => {
            return res.json({ subtitles })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    addSubtitle: function(req, res) {
        const { id } = req.params;
        const { text, startTime, endTime, speakerProfile } = req.body;
        console.log(req.body)
        subtitlesService.addSubtitle(id, { text, startTime, endTime, speakerProfile })
        .then(() => {
            return subtitlesService.findById(id);
        })
        .then((subtitles) => {
            return res.json({ subtitles });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    updateSubtitle: function(req, res) {
        const { id, subtitlePosition } = req.params;
        const changes = req.body;
        let subtitles;
        subtitlesService.updateSubtitle(id, subtitlePosition, changes)
        .then((changes) => {
            return res.json({ position: subtitlePosition, ...changes });
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    activateSubtitles: function(req, res) {
        const { id } = req.params;
        const { activated } = req.body;
        let subtitles;
        let translationExport;
        subtitlesService.findById(id)
        .then((subtitlesDoc) => {
            if (!subtitlesDoc) throw new Error('Invalid id');
            subtitles = subtitlesDoc.toObject();
            return subtitlesService.updateById(id, { activated })
        })
        .then(() => translationExportService.find({ article: subtitles.article }).sort({ created_at: -1 }).limit(1))
        .then((translationExports) => {
            if (translationExports && translationExports.length > 0) {
                translationExport = translationExports[0].toObject();
                exporterWorker.burnTranslatedArticleVideoSubtitle(translationExport._id);
                return translationExportService.updateById(translationExport._id, { subtitleUrl: '', subtitledVideoUrl: '', subtitleProgress: 10, subtitledVideoProgress: 10 });
            }
            return Promise.resolve()
        })
        .then(() => {
            return res.json({ activated })
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    deleteSubtitle: function(req, res) {
        const { id, subtitlePosition } = req.params;
        subtitlesService.deleteSubtitle(id, parseInt(subtitlePosition))
        .then(() => subtitlesService.findById(id))
        .then((subtitles) => {
            return res.json({ subtitles })
        })
        .catch((err) => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },
}

module.exports = controller;
