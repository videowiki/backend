const uuid = require('uuid').v4;
const apiKeyService = require('../shared/services/apiKey');
const userService = require('../shared/services/user');
const organizationService = require('../shared/services/organization');

const controller = {
    create: function(req, res) {
        const { organization, permissions, origins } = req.body;
        let newApiKey = {};
        let orgRole = {
            organization,
            organizationOwner: false,
            permissions,
            inviteStatus: 'accepted',
            registerMethod: 'api',
            inviteToken: `${uuid()}_${uuid()}`,
        };

        let newUser = {
            firstname: uuid(),
            lastname: uuid(),
            email: uuid(),
            emailVerified: true,
            apiUser: true,
            organizationRoles: [orgRole]
        };

        userService.create(newUser)
        .then((userData) => {
            newApiKey = {
                organization,
                user: userData._id,
                key: apiKeyService.generateApiKey(),
                origins,
                active: true,
            }
            return apiKeyService.create(newApiKey)
        })
        .then(apiKeyData => {
            newApiKey = apiKeyData.toObject();
            // Update organization origins
            return organizationService.findById(organization)
        })
        .then((organization) => {
            console.log(organization)
                let newOrigins = (organization.origins || []).slice();
                origins.forEach(origin => {
                    if (newOrigins.indexOf(origin) === -1) {
                        newOrigins.push(origin);
                    }
                });
                return organizationService.updateById(organization._id, { origins: newOrigins })
        })
        .then(() => apiKeyService.findById(newApiKey._id).populate('user'))
        .then(apiKeyData => res.json({ apiKey: apiKeyData.toObject() }))
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message)
        })
    },

    get: function(req, res) {
        const { organization } = req.query;
        apiKeyService
        .find({ organization, userKey: { $ne: true } })
        .populate('user')
        .then(apiKeys => {
            return res.json({ apiKeys })
        })
        .catch(err => {
            console.log(err)
            return res.status(400).send('Something went wrong')
        })
    },

    delete: function(req, res) {
        const { apiKeyId } = req.params;
        let apiKey;
        apiKeyService.findById(apiKeyId)
        .then((apiKeyDoc) => {
            apiKey = apiKeyDoc.toObject();
            return userService.remove({ _id: apiKeyDoc.user })
        })
        .then(() => apiKeyService.remove({ _id: apiKeyId}))
        .then(() => res.json({ apiKey }))
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },

    getApiKeyByKey: function(req, res) {
        const { apiKey } = req.query;
        apiKeyService.findOne({ key: apiKey }).populate('organization')
        .then((apiKey) => {
            return res.json({ apiKey });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Something went wrong');
        })
    },

    getUserOrganizationKey: function(req, res) {
        const user = req.user;
        const { organization } = req.query;
        console.log('user', user._id, organization)
        apiKeyService.findOne({ user: user._id, organization })
        .then((apiKey) => {
            if (!apiKey) {

            } else {
                return res.json({ apiKey })
            }
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    }
}

module.exports = controller;