const userService = require('../shared/services/user');
const organizationService = require('../shared/services/organization');
const apiKeyService = require('../shared/services/apiKey');
const ALLOWED_PERMISSIONS = ['admin', 'translate', 'review'];

const middlewares = {
    authorizeOrganizationAdmin: content => function (req, res, next) {
        const { organization } = req[content];
        const userRoles = req.user.organizationRoles;
        if (userRoles && userRoles.length > 0) {
            const orgRole = userRoles.find(u => u.organization._id.toString() === organization.toString());
            if (orgRole && (orgRole.organizationOwner || orgRole.permissions.indexOf('admin') !== -1)) {
                return next();
            }
        }
        return res.status(401).send('Unauthorized')
    },
    authorizeDeletekey: function (req, res, next) {
        const { apiKeyId } = req.params;
        apiKeyService.findById(apiKeyId)
            .then((apiKey) => {
                if (!apiKey) throw new Error('Invalid api key');
                const organization = apiKey.organization;
                const userRoles = req.user.organizationRoles;
                if (userRoles && userRoles.length > 0) {
                    const orgRole = userRoles.find(u => u.organization._id.toString() === organization.toString());
                    if (orgRole && (orgRole.organizationOwner || orgRole.permissions.indexOf('admin') !== -1)) {
                        return next();
                    }
                }
                return res.status(401).send('Unauthorized')
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    },
    validateOrigins: function (req, res, next) {
        let { origins } = req.body;
        origins = origins.map(o => {
            let newOrg = o;
            if (newOrg.indexOf('https://') !== -1) {
                newOrg.replace('https://', '')
            } else if (newOrg.indexOf('http://') !== -1) {
                newOrg.replace('http://', '');
            }
            return newOrg.toLowerCase();
        })
        let valid = true;
        origins.forEach(origin => {
            if (origin.split('.').length < 2) {
                valid = true;
            }
        });
        if (valid) return next();
        return res.status(400).send('Invalid origin format: website.com|website.org');
    },
    validatePermissions: function (req, res, next) {
        const { permissions } = req.body;
        let valid = true;
        // VALIDATE PERMISSIONS
        permissions.forEach(p => {
            if (ALLOWED_PERMISSIONS.indexOf(p) === -1) {
                valid = false;
            }
        });
        if (valid) return next();
        return res.status(400).send('Invalid permissions: ' + ALLOWED_PERMISSIONS.join('|'));
    },
}

module.exports = middlewares;