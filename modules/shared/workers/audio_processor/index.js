const superagent = require('superagent');
const articleService = require('../../services/article');
const noiseCancellationVideoService = require('../../services/noiseCancellationVideo');
const rabbitmqService = require('../../vendors/rabbitmq');
const queues = require('../../vendors/rabbitmq/queues');

const websocketsService = require('../../vendors/websockets');
const websocketsRooms = require('../../vendors/websockets/rooms');
const events = require('../../vendors/websockets/events');

const AUDIO_PROCESSOR_API_ROOT = process.env.AUDIO_PROCESSOR_API_ROOT;

let audioProcessorChannel;
if (!audioProcessorChannel) {
    console.log('####### Starting audio processor channel #######');
    rabbitmqService.createChannel(process.env.RABBITMQ_SERVER, (err, ch) => {
        if (err) {
            console.log('error creating channel for exporter', err);
        } else if (ch) {
            audioProcessorChannel = ch;
            console.log('Connected to rabbitmq audio processor server successfully');
            ch.consume(queues.PROCESS_RECORDED_AUDIO_FINISHED_QUEUE, onProcessRecordedAudioFinish, { noAck: false });
            ch.consume(queues.PROCESS_NOISECANCELLATIONVIDEO_AUDIO_FINISHED_QUEUE, onProcessNoiseCancellationFinish, { noAck: false });
        }
    })
}

/*
  Respnse on finish queue consists of
    success: flag of success.
    articleId,
    slidePosition,
    subslidePosition
    Just forward to the user if he's online
*/
function onProcessRecordedAudioFinish(msg) {
    const { success, articleId, slidePosition, subslidePosition } = JSON.parse(msg.content.toString());
    audioProcessorChannel.ack(msg);
    let article;
    let subslide;
    let audio;
    articleService.findById(articleId)
        .then((a) => {
            article = a
            subslide = article.slides.find(s => parseInt(slidePosition) === parseInt(s.position)).content.find((s) => parseInt(subslidePosition) === parseInt(s.position));
            audio = subslide.audio;
            websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(article.organization)).emit(`${events.RECORDED_AUDIO_PROCESSED}/${articleId}`, { success, slidePosition, subslidePosition, audio });
        })
        .catch((err) => {
            console.log(err);
        })

}

function onProcessNoiseCancellationFinish(msg) {
    const { noiseCancellationVideoId } = JSON.parse(msg.content.toString());
    audioProcessorChannel.ack(msg);
    noiseCancellationVideoService.findById(noiseCancellationVideoId)
    .then((doc) => {
        const noiseCancellationVideo = doc.toObject()
        websocketsService.ioEmitter.to(websocketsRooms.getOrganizationRoom(noiseCancellationVideo.organization)).emit(`${events.NOISE_CANCELLATION_VIDEO_FINISH}`, { noiseCancellationVideo });
        
    })
    .catch(err => {
        console.log('onProcessNoiseCancellationFinish error', err);
    })
}

function processRecordedAudio(identifier) {
    audioProcessorChannel.sendToQueue(queues.PROCESS_RECORDED_AUDIO_QUEUE, new Buffer(JSON.stringify(identifier)), { persistent: true });
}

function processNoiseCancellationVideo(identifier) {
    audioProcessorChannel.sendToQueue(queues.PROCESS_NOISECANCELLATIONVIDEO_AUDIO_QUEUE, new Buffer(JSON.stringify(identifier)), { persistent: true });
}

function processRecordedAudioViaApi(articleId, slidePosition, subslidePosition) {
    return new Promise((resolve, reject) => {
        superagent.post(`${AUDIO_PROCESSOR_API_ROOT}/process_audio`, { articleId, slidePosition, subslidePosition })
        .then((res) => {
            resolve(res.body);
        })
        .catch(err => {
            reject(err);  
        })
    })
}

// Test connection with audio processor 
superagent.get(`${AUDIO_PROCESSOR_API_ROOT}/`)
.then((res) => {
    console.log('audio processor check')
    console.log(res.body);
})
.catch(err => {
    console.log(AUDIO_PROCESSOR_API_ROOT)
    console.log('audio processor check failed', err);
})

module.exports = {
    processRecordedAudio,
    processRecordedAudioViaApi,
    processNoiseCancellationVideo,
}