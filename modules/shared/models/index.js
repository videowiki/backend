const mongoose = require('mongoose');
const { SchemaNames } = require('./Schemas/utils/schemaNames');

const ArticleSchemas = require('./Schemas/Article');
const CommentSchemas = require('./Schemas/Comment');
const OrganizationSchemas = require('./Schemas/Organization');
const TranslationRequestSchemas = require('./Schemas/TranslationRequest');
const UserSchemas = require('./Schemas/User');
const VideoSchemas = require('./Schemas/Video');
const NoiseCancellationVideoSchemas = require('./Schemas/NoiseCancellationVideo');
const SocketConnectionSchemas = require('./Schemas/SocketConnection');
const TranslationExportSchemas = require('./Schemas/TranslationExport'); 
const NotifiationSchemas = require('./Schemas/Notification');
const SubtitlesSchemas = require('./Schemas/Subtitles');
const ApiDocsSubscriberSchemas = require('./Schemas/ApiDocsSubscriber')
const ApiKeySchemas = require('./Schemas/ApiKey')
const VideoTutorialContributionSchemas = require('./Schemas/VideoTutorialContribution')

const Article = mongoose.model(SchemaNames.article, ArticleSchemas.ArticleSchema);
// const CommentsThread = mongoose.model(SchemaNames.commentsThread, CommentsThreadSchemas.CommentsThread);
const Comment = mongoose.model(SchemaNames.comment, CommentSchemas.Comment);
const Organization = mongoose.model(SchemaNames.organization, OrganizationSchemas.OrganizationSchema);
const TranslationRequest = mongoose.model(SchemaNames.translationRequest, TranslationRequestSchemas.TranslationRequestSchema);
const User = mongoose.model(SchemaNames.user, UserSchemas.UserSchema);
const Video = mongoose.model(SchemaNames.video, VideoSchemas.VideoSchema);
const NoiseCancellationVideo = mongoose.model(SchemaNames.noiseCancellationVideo, NoiseCancellationVideoSchemas.NoiseCancellationVideoSchema);
const OrgRole = mongoose.model(SchemaNames.orgRole, UserSchemas.OrganizationRoleSchema);
const SocketConnection = mongoose.model(SchemaNames.socketConnection, SocketConnectionSchemas.SocketConnectionSchema)
const TranslationExport = mongoose.model(SchemaNames.translationExport, TranslationExportSchemas.TranslationExport);
const Notification = mongoose.model(SchemaNames.notification, NotifiationSchemas.NotificationSchema);
const Subtitles = mongoose.model(SchemaNames.subtitles, SubtitlesSchemas.SubtitlesSchema);
const ApiDocsSubscriber = mongoose.model(SchemaNames.apiDocsSubscriber, ApiDocsSubscriberSchemas.ApiDocsSubscriber)
const ApiKey = mongoose.model(SchemaNames.apiKey, ApiKeySchemas.ApiKey)
const VideoTutorialContribution = mongoose.model(SchemaNames.videoTutorialContribution, VideoTutorialContributionSchemas.VideoTutorialContributionSchema)

module.exports = {
    User,
    Video,
    Article,
    OrgRole,
    Comment,
    Organization,
    Notification,
    Subtitles,
    TranslationExport,
    SocketConnection,
    TranslationRequest,
    ApiDocsSubscriber,
    NoiseCancellationVideo,
    ApiKey,
    VideoTutorialContribution,
};
