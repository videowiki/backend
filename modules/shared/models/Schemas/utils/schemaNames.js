const SchemaNames = {
    commentsThread: 'commentsThread',
    article: 'article',
    organization: 'organization',
    translationRequest: 'translationRequest',
    user: 'user',
    video: 'video',
    orgRole: 'orgRole',
    socketConnection: 'socketConnection',
    translationExport: 'translationExport',
    notification: 'notification',
    comment: 'comment',
    subtitles: 'subtitles',
    apiDocsSubscriber: 'apiDocsSubscriber',
    noiseCancellationVideo: 'noiseCancellationVideo',
    apiKey: 'apiKey',
    videoTutorialContribution: 'videoTutorialContribution',
};

module.exports = {
    SchemaNames,
};
