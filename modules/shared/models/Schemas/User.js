const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { SchemaNames } = require('./utils/schemaNames');

const USER_PERMISSIONS_ENUM = ['admin', 'review', 'translate'];
const REGISTER_METHOD_ENUM = ['api', 'email', 'social', 'invite'];
const INVITE_STATUS_ENUM = ['pending', 'accepted', 'declined'];

const OrganizationRoleSchema = new Schema({
    organization: { type: Schema.Types.ObjectId, ref: SchemaNames.organization },
    organizationOwner: { type: Boolean, default: false },
    permissions: [{ type: String, enum: USER_PERMISSIONS_ENUM }],
    inviteStatus: { type: String, enum: INVITE_STATUS_ENUM, default: 'pending' },
    inviteToken: String,
});

const UserSchema = new Schema({
    firstname: { type: String },
    lastname: { type: String },
    languages: [{ type: String }],
    email: { type: String, unique: true },
    password: { type: String, select: false },
    
    passwordSet: { type: Boolean, default: false },

    emailVerified: { type: Boolean, default: false },
    verifyToken: { type: String }, 
    registerMethod: { type: String, enum: REGISTER_METHOD_ENUM, default: 'email'},
    
    organizationRoles: [OrganizationRoleSchema],
    resetCode: { type: String },

    apiUser: { type: Boolean, default: false },
    showUserGuiding: { type: Boolean, default: true },
});

module.exports = { UserSchema, OrganizationRoleSchema };
