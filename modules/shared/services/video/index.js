const async = require('async');
const videoHandler = require('../../dbHandlers/video');
const storageService = require('../../vendors/storage');
const BaseService = require('../BaseService');

class VideoService extends BaseService {
    constructor() {
        super(videoHandler);
    }

    createVideo(values) {
        return this.create(values)
    }
    
    deleteVideosMedia(videos) {
        const deleteKeys = [];
        const deleteFuncArray = [];
        videos.forEach(video => {
            if (video.Key) {
                deleteKeys.push(video.Key);
            }
            if (video.subtitleKey) {
                deleteKeys.push(video.subtitleKey);
            }
        });
        deleteKeys.forEach((key) => {
            deleteFuncArray.push((cb) => {
                const [directoryName, fileName] = key.split('/');
                storageService.deleteFile(directoryName, fileName)
                    .then((res) => {
                        console.log(res);
                        return cb(null, res);
                    })
                    .catch((err) => {
                        console.log(err);
                        return cb();
                    })
            })
        })
    
        async.parallelLimit(deleteFuncArray, 5, (err, result) => {
            console.log('deleted video media', err, videos, result)
        })
    }
    
}

module.exports = new VideoService();