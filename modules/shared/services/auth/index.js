const authDbHandler = require('../../dbHandlers/authentication');
const jwt = require('jsonwebtoken');
const DEFAULT_EXPIRE_TIME = '48h';
const SECRET_STRING = process.env.SECRET_STRING;

const authService = {
    tokenMap: {},

    validateRegData: async function ({ email, firstname, lastname, password, orgName }) {
        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let isEmailOk = email.match(mailformat);
        let isPasswordOk = password.length >= 8;
        let isOrgNameOk = orgName.length > 1;
        let isFirstnameOk = firstname && firstname.length > 1;
        let isLastnameOk = lastname && lastname.length > 1;

        let isUserAvailable = await this.checkUserExistence(email);
        let isOrgAvailable = await this.checkOrganizationExistence(orgName);

        if (!isEmailOk) {
            return {
                message: 'Invalid email format',
                isValid: false
            }
        }

        if (!isPasswordOk) {
            return {
                message: 'Password must be at least 8 characters',
                isValid: false
            }
        }

        if (!isOrgNameOk) {
            return {
                message: 'Invalid Organization Name',
                isValid: false
            }
        }

        if (isUserAvailable) {
            return {
                message: 'User Already Available',
                isValid: false
            }
        }

        if (isOrgAvailable) {
            return {
                message: `Organization named ${orgName} already exists, Please enter a different name`,
                isValid: false
            }
        }

        if (!isFirstnameOk) {
            return {
                message: 'First name is a required field',
                isValid: false,
            }
        }

        if (!isLastnameOk) {
            return {
                message: 'Last name is a required field',
                isValid: false,
            }
        }

        return {
            isValid: true
        };
    },

    checkUserExistence: async function (email) {
        return await authDbHandler.checkUser(email);
    },

    checkOrganizationExistence: async function(name) {
        return await authDbHandler.checkOrganization(name);
    },

    registerUser: async function ({email, firstname, lastname, password, orgName, roles}) {
        return authDbHandler.addUser({
            email, 
            password, 
            orgName, 
            firstname,
            lastname,
            organizationOwner: true
        });
    },

    loginUser: async function (email, password, temp) {
        let isUserOk = await authDbHandler.validateLogin(email, password);

        if (isUserOk) {
            return this.generateLoginToken(email, temp);
        } else {
            return false;
        }
    },

    generateLoginToken: async function (email, temp) {
        console.error(SECRET_STRING);
        return jwt.sign({ email }, SECRET_STRING, { expiresIn: temp ? '1m' : DEFAULT_EXPIRE_TIME});
    },

    generateResetToken: function(email) {

    },

    refreshToken: function(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, process.env.SECRET_STRING, (err, user) => {
                if (err) {
                    console.log('decodeApiToken - error ', err);
                    return reject(new Error('Invalid token signature'));
                }
                console.log('user is', user)
                const { email } = user;
                
                jwt.sign({ email }, SECRET_STRING, { expiresIn: DEFAULT_EXPIRE_TIME}, (err, newToken) => {
                    if (err) return reject(err);
                    return resolve({ token: newToken, data: { email }});
                })
            })
        })
    },

    logoutUser: async function (user) {
        if (user && this.tokenMap[user]) {
            this.tokenMap[user] = undefined;
            return true;
        } else {
            return false;
        }
    }
};

module.exports = authService;
