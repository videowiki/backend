const async = require('async');
const notificationHandler = require('../../dbHandlers/notification');
const storageService = require('../../vendors/storage');
const socketConnectionService = require('../socketConnection');

const websockets = require('../../vendors/websockets');

const BaseService = require('../BaseService');

class NotificationService extends BaseService {
    constructor() {
        super(notificationHandler);
    }

    notifyUser({ email, _id, organization }, data) {
        return new Promise((resolve, reject) => {
            let notificationDoc;
            this.create(data)
                .then((n) => {
                    notificationDoc = n.toObject();
                    console.log('email is', email, organization)
                    if (email) {
                        return socketConnectionService.findOne({ userEmail: email })
                    } else if (_id) {
                        return socketConnectionService.findOne({ userId: _id })
                    }
                })
                .then((socketConnection) => {
                    if (!socketConnection) return resolve(notificationDoc);
                    if (socketConnection && socketConnection.socketId && socketConnection.organization.toString() === organization.toString()) {
                        console.log('sendig notification to ', socketConnection)
                        websockets.ioEmitter.to(socketConnection.socketId).emit(websockets.events.NEW_NOTIFICATION, { notification: notificationDoc });
                    }
                    resolve(notificationDoc);
                })
                .catch(reject);
        })
    }
}


module.exports = new NotificationService();