const userDbHandler = require('../../dbHandlers/user');
const _ = require('lodash');

const BaseService = require('../BaseService');

class UserService extends BaseService {
    constructor() {
        super(userDbHandler);
    }

    async addUser(userDetail) {
        return userDbHandler.addUser(userDetail);
    }

    async removeUser(email, organizationId) {
        let userObj = await this.getUserByEmail(email);
        let userOrganizations = userObj.organizationRoles;

        _.remove(userOrganizations, (orgRole) => {
            // Should not be deleted if organization owner
            if (orgRole.organizationOwner) {
                console.log('Organization owner cannot be removed');
                return false;
            }

            return orgRole.organization.toString() === organizationId.toString();
        });

        return await userDbHandler.updateUser(email, { organizationRoles: userOrganizations });
    }

    async editOrganizationPermission(email, organizationId, permissions) {
        let userObj = await this.getUserByEmail(email);
        let userOrganizations = userObj.organizationRoles;

        const role = _.find(userOrganizations, (i) => {
            return i.organization.toString() === organizationId.toString();
        });

        role.permissions = permissions;

        return await userDbHandler.updateUser(email, { organizationRoles: userOrganizations });
    }

    async assignOrganization(user, organizationId, permissions) {
        let finalOrgRoles = [];
        if (user.organizationRoles) {
            finalOrgRoles = user.organizationRoles;
        }
        finalOrgRoles.push({
            organization: organizationId,
            organizationOwner: false,
            permissions
        })

        userDbHandler.updateUser(user.email, {
            organizationRoles: finalOrgRoles
        });
    }

    async getOrganizationUsers(organizationId) {
        return userDbHandler.find({ 'organizationRoles.organization': organizationId, apiUser: { $ne: true }});
    }

    async getUserByEmail(email) {
        return await userDbHandler.getUserByEmail(email);
    }

    async validateEmail(email) {
        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return email.match(mailformat);
    }

    async checkUserExistence(email) {
        return await userDbHandler.checkUser(email);
    }

    async getOrgId(loggedUser) {
        let orgId = '';
        let loggedUserOrganizationRoles = loggedUser.organizationRoles;

        loggedUserOrganizationRoles.forEach((organizationsRoles) => {
            if (organizationsRoles.organizationOwner === true) {
                orgId = organizationsRoles.organization
            }
        });

        return orgId;
    }
}

module.exports = new UserService();
