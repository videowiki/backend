const uuid = require('uuid').v4;
const fs = require('fs');
const sha256 = require('sha256');
const helpers = require('../shared/utils/helpers');
const authService = require('../shared/services/auth');
const userService = require('../shared/services/user');
const emailService = require('../shared/services/email');
const organizationService = require('../shared/services/organization')
const storageService = require('../shared/vendors/storage');

const controller = {
    registerUser: async function (req, res) {
        const { email, password, firstname, lastname, orgName } = req.body;
        const logo = req.files.find((f) => f.fieldname === 'logo');
        let { isValid, message } = await authService.validateRegData({email, firstname, lastname, password, orgName});

        if (isValid) {
            let registrationResult = await authService.registerUser({ email, firstname, lastname, password, orgName });
            
            if (registrationResult !== true) {
                res.status(400).send('Something went wrong');
            } else {
                if (logo) {
                    const { err, data: uploadData } = await helpers.handlePromiseReject(storageService.saveFile('logos', `${orgName}-logo-${uuid()}.${logo.filename.split('.').pop()}`, fs.createReadStream(logo.path)))
                    if (err) {
                        console.log(err);
                        return res.json({ success: true, message: 'success' });
                    }
                    const { err: orgUpdateErr } = await helpers.handlePromiseReject(organizationService.update({ name: orgName }, { logo: uploadData.url }));
                    if (orgUpdateErr) {
                        console.log(orgUpdateErr);
                    }
                    return res.json({ success: true, message: 'success' });
                } else {
                    res.json({ success: true, message: 'success' });
                }
            }
        } else {
            res.status(400).send(message);
        }
    },

    loginUser: async function (req, res) {
        let { email, password, temp } = req.body;

        if (email && password) {
            let token = await authService.loginUser(email, password, temp);
            let user = await userService.getUserByEmail(email);
            if (token) {
                res.status(200).send({ success: true, token, user });
            } else {
                res.status(400).send('Invalid email or password');
                // res.status(200).send({ success: false });
            }
        }
    },

    logoutUser: function (req, res) {
        let user = req.user.email;

        if (user) {
            let success = authService.logoutUser(user);

            if (success) {
                res.status(200).send();
            }
        }
    },
    
    resetPassword: function(req, res) {
        const { email } = req.body;

        let user;
        let resetCode;
        userService.findOne({ email })
        .then((userDoc) => {
            if (!userDoc) throw new Error('This email is not registered in our databases');
            user = userDoc.toObject();
            resetCode = sha256(uuid());
            return userService.update({ email }, { resetCode })
        })
        .then(() => {
            return emailService.resetUserPassord({ to: user, resetCode })
        })
        .then(() => {
            return res.json({ success: true });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    },

    refreshToken: function(req, res) {
        const { token } = req.body;
        let newToken;
        authService.refreshToken(token)
        .then((tokenData) => {
            const { token, data } = tokenData;
            newToken = token;
            return userService.getUserByEmail(data.email);
        })
        .then(user => {
            return res.json({ success: true, token: newToken, user: user });
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send('Invalid token');
        })
    }
};

module.exports = controller;
