const { AUTHENTICATE, AUTHENTICATE_FAILED, AUTHENTICATE_SUCCESS } = require('../shared/vendors/websockets/events');
const { getOrganizationRoom } = require('../shared/vendors/websockets/rooms');
const { SocketConnection } = require('../shared/models');
const socketConnectionService = require('../shared/services/socketConnection');
const userService = require('../shared/services/user');
const organizationService = require('../shared/services/organization');
const jwt = require('jsonwebtoken');

const handlers = [
  {
    event: AUTHENTICATE,
    handler: (socket) => (data) => {
      const { token, organization } = data;
      if (token) {
        jwt.verify(token, process.env.SECRET_STRING, (err, user) => {
          if (err) {
            console.log('decodeApiToken - error ', err);
            return socket.emit(AUTHENTICATE_FAILED);
          }
          const { email } = user;
          userService.findOne({ email })
          .then((userData) => {
            socketConnectionService.update({ userEmail: email }, { userEmail: userData.email, userId: userData._id, socketId: socket.id, organization }, { upsert: true, new: true })
            .then((socketConnection) => {
              let userData;
              userService.getUserByEmail(email)
              .then((user) => {
                if (!user) throw new Error('Invalid user email');
                userData = user;
                if (userData.organizationRoles && userData.organizationRoles.find((or) => or.organization._id.toString() === organization.toString())) {
                  // Join organization room
                  if (!socket.rooms[getOrganizationRoom(organization)]) {
                    console.log('joining organization room');
                    socket.join(getOrganizationRoom(organization));
                  }
                  return socketConnectionService.find({ userEmail: email });
                } else {
                  throw new Error('Not a member of the organization');
                }
              })
              .then((socketConnections) => {
                if (socketConnections && socketConnections.length > 0) {
                    return socket.emit(AUTHENTICATE_SUCCESS, socketConnections[0]);
                }
              })
              .catch((err) => {
                console.log(err);
                return socket.emit(AUTHENTICATE_FAILED);
              })
            })
          })
          .catch(err => {
            console.log('error authenticating user', err);
            return socket.emit(AUTHENTICATE_FAILED);
          })
        });
      } else {
        return socket.emit(AUTHENTICATE_FAILED);
      }
    },
  },
];

module.exports = {
  handlers,
}