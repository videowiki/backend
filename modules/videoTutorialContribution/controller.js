const fs = require('fs');
const async = require('async');
const storageVendor = require('../shared/vendors/storage');
const videoTutorialContributionService = require('../shared/services/videoTutorialContribution');
const emailService = require('../shared/services/email');

const fileUtils = require('../shared/utils/fileUtils');


const controller = {
    uploadVideo: function (req, res) {
        // 1- Create new video instance
        // 2- Upload file to s3
        // 3- Send to the exporter to transcribe it
        const { title, url } = req.body;
        let file = req.files &&  req.files.find((f) => f.fieldname === 'video');
        let uploadFilePromise;
        if (file) {
            uploadFilePromise = storageVendor.saveFile('videoTutorialContributions', file.filename, fs.createReadStream(file.path))
        } else if (url) {
            uploadFilePromise = new Promise((resolve, reject) => {
                fileUtils.downloadFile(url)
                .then((filePath) => {
                    newFilePath = filePath;
                    file = { path: newFilePath };
                    return storageVendor.saveFile('videoTutorialContributions', filePath.split('/').pop(), fs.createReadStream(filePath))
                })
                .then((data) => {
                    return resolve(data);
                })
                .catch(reject)
            })
        } else {
            return res.status(400).send('Please upload video file or a video url')
        }
        let video;
        const videoData = {
            title,
        }
        console.log('before upload', file)
        videoTutorialContributionService.create(videoData)
        .then((doc) => {
            video = doc.toObject();
            console.log('doc created', video)
            return uploadFilePromise
        })
        .then((result) => {
            console.log(' =============== uploaded ====================');
            fs.unlink(file.path, () => { });
            const { url, data } = result;
            const Key = data.Key;
            video.Key = Key;
            video.url = url;
            video.status = 'uploaded';
            console.log('uploaed', result, video)
            res.json(video);
            console.log('created video', video)
            return videoTutorialContributionService.update({ _id: video._id }, { Key, url })
        })
        .then(() => {
            // Notify Pratik and Hassan to confirm and publish the video
            ['pratik.shetty@tlrfindia.com', 'h.m.amin994@gmail.com'].forEach(to => {
                emailService.sendVideoContributionUploadedMessage({ to, content: `A new video contribution was uploaded with title ${video.title} on the url ${video.url}`})
                .then(() => {
                    console.log('sent message to ', to);
                })
                .catch(err => {
                    console.log('erro sending email to publish video contribution')
                })
            })
        })
        .catch(err => {
            console.log(err);
            fs.unlink(file.path, () => { });
            videoTutorialContributionService.update({ _id: video._id }, { status: 'failed' });
        })
    },

    getVideos: function(req, res) {
       videoTutorialContributionService.find({ published: true })
       .then((videos) => {
            return res.json({ videos });
       })
       .catch(err => {
           console.log(err)
           return res.status(400).send('Something went wrong');
       })
    },


}

module.exports = controller;