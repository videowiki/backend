const controller = require('./controller');
const path = require('path');
// const validation = require('./validate_request');
// const middlewares = require('./middlewares');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})

const upload = multer({ storage: storage })

const mount = function (router) {
    // router.post("/remove", controller.removeUser);
    // router.post('/editPermissions', controller.editPermissions);
    router.post('/', upload.any(), controller.uploadVideo)

    router.get('/', controller.getVideos)
    return router;
};

module.exports = {
    mount,
};
